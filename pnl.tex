\chapter{Programmazione non lineare}
\addtothm{Programmazione non lineare}

%Teoremi come weierstrass e fermat
%Funzioni convesse, coercive
%Quadratiche
%Classificazione di punti stazionari interni tramite la matrice hessiana
%Teorema LKKT e ricerca delle possibili soluzioni del relativo sistema
%Classificazione delle soluzioni del LKKT
%Studio locale


Un problema di programmazione non lineare consiste nel trovare il minimo o il massimo di una funzione all'interno di un dominio definito da vincoli di tipo non necessariamente lineare. Faremo sempre riferimento a un problema standard nella forma
\begin{equation} \label{eq:stdpnl} \begin{split}
\min_\Omega\ f(x) \qquad \Omega = \{x \in \R^n : g(x) \leqslant 0,\ h(x) = 0 \} \\
g : \R^n \to \R^m,\ h : \R^n \to \R^p\ \mathrm{e\ } g,h \in C^1(\Omega)
\end{split} \end{equation}

Come vedremo più avanti, l'insieme $\Omega$ può essere o non essere limitato e/o convesso, ma comunque sempre chiuso, per come è costruito.

Il teorema di Weierstrass fornisce una condizione sufficiente per l'esistenza di minimi e massimi globali:

\begin{teorema}[Weierstrass]
Se $\Omega \subset \R^n$ è chiuso e limitato e $f : \Omega \to \R$ è continua, allora esistono il massimo e il minimo (globali) di $f$ su $\Omega$.
\end{teorema}

\begin{definizione}[Funzione coerciva]
Una funzione $f : \R^n \to \R$ si dice coerciva se 
$$ \lim_{\|x\| \to +\infty}\ f(x) = +\infty $$
\end{definizione}
Un aspetto interessante delle funzioni continue e coercive è che, su un insieme chiuso (limitato o non limitato), ammettono sempre un minimo globale.

\section{Ricerca e classificazione di punti stazionari interni a $\Omega$}

\begin{teorema}[Fermat]
Se $\bar x$, punto interno di $\Omega$, è un punto di minimo o di massimo locale di $f$ in $\Omega$, allora $\nabla f(\bar x) = 0$.
\end{teorema}

Il teorema di Fermat fornisce una condizione necessaria per la ricerca di minimi e massimi locali. Infatti, tutti i punti interni di $\Omega$ in cui il gradiente di $f$ si azzera si chiamano punti stazionari, e ognuno di essi può essere catalogato come:
\begin{itemize} \setlength{\parskip}{0pt}
\item minimo (solo locale o anche globale)
\item massimo (solo locale o anche globale)
\item sella
\end{itemize}
Se $f$ è \textsc{non} costante, le tre categorie sono mutuamente esclusive. Ogni minimo/massimo globale è anche locale e ovviamente se esistono più punti di minimo (o massimo) globali, essi hanno tutti lo stesso valore.

\begin{teorema}[Classificazione dei punti stazionari]
Data una funzione $f \in C^2$ e un suo punto $\bar x$ stazionario ($\nabla f(\bar x) = 0$), valgono le seguenti affermazioni:
\begin{itemize}
\item $\bar x$ è minimo locale $\Longrightarrow\ \H f(\bar x) \geqslant 0\footnotemark$. \footnotetext{Con la notazione $\H f(x) \geqslant 0$ si intende che la matrice hessiana di $f$ è semidefinita positiva, cioè ha autovalori tutti non negativi. Formalmente, una matrice $A$ si dice semidefinita positiva se $\trasp x A x \geqslant 0,\ \forall x \neq 0$.}
\item $\bar x$ è massimo locale $\Longrightarrow\ \H f(\bar x) \leqslant 0$.
\item $\H f(\bar x) > 0\footnotemark \Longrightarrow\ \bar x$ è minimo locale. \footnotetext{Con la notazione $\H f(x) > 0$ si intende che la matrice hessiana di $f$ è definita positiva, cioè ha autovalori tutti strettamente positivi. Formalmente, una matrice $A$ si dice definita positiva se $\trasp x A x > 0,\ \forall x \neq 0$. Spesso l'hessiana è indicata anche con ``$\nabla^2 f(x)$''.}
\item $\H f(\bar x) < 0 \Longrightarrow\ \bar x$ è massimo locale.
\item $\H f(\bar x)$ è indefinita\footnote{Una matrice si dice indefinita se non è né semidefinita positiva né semidefinita negativa. Essa possiede sia autovalori positivi ($> 0$) sia negativi ($< 0$).} $\Longleftrightarrow\ \bar x$ è un punto di sella.
\end{itemize}
\end{teorema}

Il teorema fornisce alcune condizioni sufficienti e alcune necessarie per poter collocare un punto stazionario in una o più delle tre categorie elencate sopra. Tipicamente, trovato un punto stazionario, si procede provando prima a vedere se una delle condizioni sufficienti (cioè $\H f(\bar x) > 0$, $\H f(\bar x) < 0$ o $\H f(\bar x)$ indefinita) è soddisfatta. Se così fosse, potremmo catalogare direttamente $\bar x$ in una sola delle tre categorie. In caso contrario, invece, si procede per esclusione: infatti, se l'hessiana non è né definita né indefinita, possiamo dire con certezza che $\bar x$ non è un punto di sella e quindi sarà per forza un minimo o un massimo locale (o, se $f$ fosse costante, entrambi). Per discriminare i due casi è sufficiente trovare una direzione spostandosi lungo la quale (partendo da $\bar x$)

\begin{definizione}[Funzione convessa] \addtocounter{equation}{1}
Una funzione $f : \R^n \to \R$ si dice convessa se per ogni coppia di punti $a,b \in \R^n$ si ha che
\begin{equation} \label{eq:convessità} \tag{\theequation .A}
f(\lambda a + (1-\lambda)\: b) \leqslant \lambda\: f(a) + (1-\lambda)\: f(b), \quad \forall \lambda \in [0,1]
\end{equation}

Se $f$ è differenziabile, la condizione di convessità si può esprimere usando il gradiente:
\begin{equation} \label{eq:convessitàdiff} \tag{\theequation .B}
f(b) \geqslant f(a) + \nabla f(a)\:(b-a), \quad \forall a,b \in \R^n
\end{equation}
% $$ f(x) = 1 + \nicefrac{1}{5} \left(x - \nicefrac{3}{2}\right)^2 \qquad f'(x) = \nicefrac{2}{5} x - \nicefrac{3}{5} $$

Se $f \in C^2$, la convessità si può determinare usando le derivate parziali di secondo ordine:
\begin{equation} \label{eq:convessitàhess} \tag{\theequation .C}
\matrice{cc}{
	\frac{\partial^2 f}{\partial x^2} & \frac{\partial^2 f}{\partial x \partial y} \\
	\frac{\partial^2 f}{\partial y \partial x} & \frac{\partial^2 f}{\partial y^2}
} = \H f \geqslant 0, \quad \forall x \in \R^n
\end{equation}
\end{definizione}

$f(\lambda\: a + (1-\lambda)\: b)$ rappresenta il grafico della funzione tra i punti $a$ e $b$, mentre $\lambda\: f(a) + (1-\lambda)\: f(b)$ rappresenta il segmento (chiamato corda) che congiunge i punti sul grafico in corrispondenza rispettivamente di $a$ e $b$.

$f(a) + \nabla\: f(a)\: (b - a)$ rappresenta il piano (la retta) tangente al grafico passante per il punto $(a, f(a))$.

Notare che la \eqref{eq:convessitàhess}, nel caso di funzioni di una sola variabile, diventa $f''(x) \geqslant 0,\ \forall x \in \R$.

Due grafici di esempio di una funzione convessa di una variabile possono chiarire meglio le espressioni matematiche:
\begin{center}
\begin{minipage}{0.49\textwidth} \centering
	\begin{tikzpicture}[>=latex']
	\draw[thick,->] (-.3,0) -- (4,0) node [anchor=west] {$x$};
	%\draw[thick,->] (0,-.3) -- (0,3) node [anchor=south] {$y$};
	%\draw (-.6,2) parabola bend (2,1) (4.3,1.5);
	\draw[domain=-.3:4.3] plot (\x,{1 + .2*(\x - 1.5)*(\x - 1.5)}) node[above] {$f(x)$};
	\coordinate[label=below:$a$] (a) at (1,0); \fill (a) circle (.5mm);
	\coordinate[label=below:$b$] (b) at (3,0); \fill (b) circle (.5mm);
	\coordinate (fa) at (1,1.05); \fill (fa) circle (.5mm);
	\coordinate (fb) at (3,1.45); \fill (fb) circle (.5mm);
	\draw[thin,densely dotted] (a) -- (fa) (b) -- (fb);
	\draw (fa) -- (fb) node[midway,sloped,above] {$\lambda\: f(a) + (1-\lambda)\: f(b)$};
	\end{tikzpicture} \\
	\eqref{eq:convessità}
\end{minipage}
\begin{minipage}{0.49\textwidth} \centering
	\begin{tikzpicture}[>=latex']
	\draw[thick,->] (-.3,0) -- (4,0) node [anchor=west] {$b$};
	%\draw[thick,->] (0,-.3) -- (0,3) node [anchor=south] {$y$};
	%\draw (-.6,2) parabola bend (2,1) (4.3,1.5);
	\draw[domain=-.3:4.3] plot (\x,{1 + .2*(\x - 1.5)*(\x - 1.5)}) node[above] {$f(b)$};
	\coordinate[label=below:$a$] (a) at (1,0); \fill (a) circle (.5mm);
%	\coordinate[label=below:$b$] (b) at (3,0); \fill (b) circle (.5mm);
	\coordinate (fa) at (1,1.05); \fill (fa) circle (.5mm);
%	\coordinate[label=above:$f(b)$] (fb) at (3,1.45); \fill (fb) circle (.5mm);
	\draw (-.6,1.37) -- (4.6,.33) node [pos=.4,sloped,below] {$f(a) + f'(a)\: (b - a)$};
	\draw[thin,densely dotted] (a) -- (fa);% (b) -- (fb);
	\end{tikzpicture} \\
	\eqref{eq:convessitàdiff}
\end{minipage}
\end{center}

L'aspetto interessante delle funzioni continue e convesse è che qualsiasi loro punto stazionario è un minimo globale.

\subsection{Funzioni quadratiche}

\begin{definizione}
Una funzione quadratica $f : \R^n \to \R$ si presenta nella forma
\begin{equation} \label{eq:quadratica}
f(x) = \frac{1}{2} \trasp x A\: x + \trasp b x + c, \qquad c \in \R,\ b,x \in \R^n,\ A \in \R^{n\times n}\ \mathrm{simmetrica}
\end{equation}
\end{definizione}
Spesso il termine noto $c$ viene tralasciato perché ininfluente al fine di trovare i punti di massimo o minimo. Il gradiente e la matrice hessiana della \eqref{eq:quadratica} sono piuttosto comodi da scrivere:
$$ \nabla f(x) = A\: x + b \qquad \H f(x) = A $$
Come si vede, la matrice hessiana di una quadratica è sempre costante. Notare che la condizione di simmetricità di $A$ è essenziale perché sappiamo, grazie al teorema di Schwartz, che la matrice hessiana di una funzione è sempre simmetrica (questo implica inoltre che tutti i suoi autovalori sono reali).

\begin{esempio}
Prendiamo una quadratica di esempio e verifichiamo quanto visto:
$$ A = \matrice{cc}{ -4 & 5 \\ 5 & 0 } \quad b = \vettore{ 7 \\ 4 } \quad c = \sqrt 2 \qquad
%f(x) = \frac{1}{2} \matrice{cc}{ x_1 & x_2 } \matrice{cc}{ -4 & 5 \\ 5 & 0 } \vettore{x_1 \\ x_2} + \matrice{cc}{ 7 & 4 } \vettore{x_1 \\ x_2} + \sqrt 2
%f(x) = \frac{1}{2} \matrice{cc}{x_1 & x_2} \vettore{ -4 x_1 + 5 x_2 \\ 5 x_1 } + 7 x_1 + 4 x_2 + \sqrt 2
f(x) = -2 x_1^2 + 5 x_1 x_2 + 7 x_1 + 4 x_2 + \sqrt 2 $$
$$ \nabla f = \vettore{ -4 x_1 + 5 x_2 + 7 \\ 5 x_1 + 4 } \qquad \H f = \matrice{cc}{ -4 & 5 \\ 5 & 0} $$
La funzione non è coerciva perché non ammette limite per $\|x\| \to +\infty$. Vediamo se è convessa, calcolando gli autovalori di $A$:
$$ (-4 -\lambda) (-\lambda) -25 = \lambda^2 + 4 \lambda - 25 = 0 \qquad \lambda_{1,2} = -2 \pm \sqrt{29} \ngeqslant 0 \longrightarrow\ \mathrm{no} $$
\end{esempio}

\subsection{Regolarità dei vincoli}

La condizione di regolarità dei vincoli è necessaria per la presentazione delle condizioni necessarie di primo ordine. Formalmente, un vincolo si dice regolare in un punto se in quel punto il cono tangente e il cono delle direzioni ammissibili del primo ordine coincidono. Per evitare di introdurre le pesanti definizioni di questi termini, ci limitiamo ad enunciare alcune condizioni sufficienti per la regolarità dei vincoli che definiscono un insieme $\Omega$:

\begin{teorema}[Condizioni sufficienti di regolarità dei vincoli]
I vincoli della \eqref{eq:stdpnl} sono regolari se si verifica almeno una delle seguenti condizioni:
\begin{enumerate}
\item tutte le funzioni $g_i, h_j$ sono lineari;
\item tutte le funzioni $g_i$ sono convesse, tutte le $h_j$ sono lineari (ovvero $\Omega$ è convesso) ed esiste $\bar x$ tale che $g(\bar x) < 0$ e $h(\bar x) = 0$ (ovvero esiste un punto interno) --- dette anche ``condizioni di Slater'';
\item i gradienti dei vincoli attivi in ogni punto $\bar x \in \Omega$, cioè
$$ \sistema[l @{\quad} l]{
	\nabla g_i(\bar x) & \forall i : g_i(\bar x) = 0 \\
	\nabla h_j(\bar x) & %\forall j : h_j(\bar x) = 0
} $$
sono linearmente indipendenti
\end{enumerate}
\end{teorema}

\begin{teorema}[Lagrange--Karush--Kuhn--Tucker] \label{teo:lkkt}
Sia $x^*$ un punto di minimo di $f$ su $\Omega$ e siano i vincoli $g(x) \leqslant 0,\ h(x) = 0$ regolari, allora esistono $\lambda \in \R^m : \lambda \geqslant 0,\ \mu \in \R^p$ tali che
\begin{equation} \label{eq:lkkt} \tag{LKKT}
\sistema{
	\nabla f(x^*) + \lambda\: \nabla g(x^*) + \mu\: \nabla h(x^*) = 0 \\
	\lambda\: g(x^*) = 0 \\
	h(x^*) = 0
}
\end{equation}
\end{teorema}

Esiste il teorema speculare del \ref{teo:lkkt} per i punti di massimo, che differisce soltanto per la condizione $\lambda \leqslant 0$.

Il \thref{teo:lkkt} fornisce una condizione necessaria perché un punto $x^*$, interno o sulla frontiera di $\Omega$, sia un minimo/massimo locale. Risolvendo il sistema \eqref{eq:lkkt} tenendo $x, \lambda, \mu$ come incognite si potrebbero ottenere alcune soluzioni che non sono ammissibili perché $x$ non appartiene a $\Omega$ oppure che non sono punti di minimo o massimo, ma selle.

\begin{esempio}
$$ 
\begin{array}{l}
	f(x_1,x_2) = x_1^2 + 3 x_2^2 \\
	g_1(x_1,x_2) = - x_1^2 - x_2^2 + 1 \\
	g_2(x_1,x_2) = -4 x_2^2 + 1
\end{array}
\qquad
\sistema{
	\min\ x_1^2 + 3 x_2^2 \\
	- x_1^2 - x_2^2 + 1 \leqslant 0 \\
	-4 x_2^2 + 1 \leqslant 0
}
\qquad
\begin{array}{l}
\nabla f = (2 x_1, 6 x_2) \\
\nabla g_1 = (-2 x_1, -2 x_2) \\
\nabla g_2 = (0, -8 x_2)
\end{array}
$$
\begin{center} \begin{tikzpicture}[>=latex']
	\tikzaxes{-3}{3}{-2}{2}
	
	\fill[pattern=north east lines] (-3,.5) -- ({cos(150)},.5) arc[x radius=1cm,y radius=1cm,start angle=150,end angle=30] -- (3,.5) -- (3,2) -- (-3,2) -- cycle;
	\draw[very thick]  (-3,.5) -- ({cos(150)},.5) arc[x radius=1cm,y radius=1cm,start angle=150,end angle=30] -- (3,.5);
	
	\fill[pattern=north east lines] (-3,-.5) -- ({cos(150)},-.5) arc[x radius=1cm,y radius=1cm,start angle=210,end angle=330] -- (3,-.5) -- (3,-2) -- (-3,-2) -- cycle;
	\draw[very thick]  (-3,-.5) -- ({cos(150)},-.5) arc[x radius=1cm,y radius=1cm,start angle=210,end angle=330] -- (3,-.5);
	
	% curve di livello
	\draw[densely dotted,semithick] 
	(0,0) ellipse[x radius={sqrt(3)/4},y radius=.25]
		  ellipse[x radius={sqrt(3)/2},y radius=.5]
		  ellipse[x radius={sqrt(3/2)},y radius={sqrt(2)/2}]
		  ellipse[x radius={sqrt(3)},y radius=1]
		  ellipse[x radius={3*sqrt(3)/2},y radius=1.5];
\end{tikzpicture} \end{center}
Risolviamo il sistema LKKT:
$$ \sistema[l @{\quad} l]{
	\circled 1 & 2 x_1 - 2 \lambda_1 x_1 = 0 \\
	\circled 2 & 6 x_2 - 2 \lambda_1 x_2 - 8 \lambda_2 x_2 = 0 \\
	\circled 3 & \lambda_1\: (- x_1^2 - x_2^2 + 1) = 0 \\
	\circled 4 & \lambda_2\: (-4 x_2^2 + 1) = 0
} $$
\begin{center}
	\begin{tikzpicture}
	[>=latex',grow'=right,label distance=.5cm,parent anchor=east,child anchor=west,
level/.style={sibling distance=3cm/#1},
level 5/.style={level distance=1cm,anchor=west},
level 2/.style={level distance=3cm},
level 1/.style={level distance=2cm}]
	
	\tikzstyle{every label}=[draw,shape=circle,inner sep=.5pt,label position=right]
	\tikzstyle{eq}=[draw,shape=circle,inner sep=.5pt,above=1pt]
	\tikzstyle{every child node}=[minimum width=1.7cm]
	\coordinate [label=1] [->]
		child {node [label=3] {$x_1 = 0$}
			child {node {$x_2 = \pm 1$}
				child {node {$\lambda_2 = 0$}
					child {node {$\lambda_1 = 3$}
						child {node {$A, B$} edge from parent [draw=none]}
						edge from parent node [eq] {2} }
					edge from parent node [eq] {4} } }
			child {node [label=2] {$\lambda_1 = 0$}
				child {node {$x_2 = 0$}
					child {node {$\lambda_2 = 0$}
						child {node {N/AMM.} edge from parent [draw=none]}
						edge from parent node [eq] {4} } }
				child {node {$\lambda_2 = \frac{3}{4}$}
					child {node {$x_2 = \pm \frac{1}{2}$}
						child {node {N/AMM.} edge from parent [draw=none]}
						edge from parent node [eq] {4} } } } }
		child {node [label=2] {$\lambda_1 = 1$}
			child {node {$x_2 = 0$}
				child {node {$x_1 = \pm 1$}
					child {node {$\lambda_2 = 0$}
						child {node {N/AMM.} edge from parent [draw=none]}
						edge from parent node [eq] {4} }
					edge from parent node [eq] {3} } }
			child {node {$\lambda_2 = \frac{1}{2}$}
				child {node {$x_2 = \pm \frac{1}{2}$}
					child {node {$x_1 = \pm \frac{\sqrt 3}{2}$}
						child {node {$C, D, E, F$} edge from parent [draw=none]}
						edge from parent node [eq] {3} }
					edge from parent node [eq] {4} } } };
	\end{tikzpicture}
	\vspace{1em} \mbox{}
	\begin{minipage}{0.59\textwidth} \centering
		\begin{tabular}{|*4{>{$}c<{$}|}}
		\hline
			\mathrm{punto} & x_1,x_2 & \lambda_1,\lambda_2 & f(x) \\
		\hline\hline
			A & 0,1 & 3,0 & 3 \\ \hline
			B & 0,-1 & 3,0 & 3 \\ \hline
			C & -\frac{\sqrt 3}{2},\frac{1}{2} & 1,\frac{1}{2} & \frac{3}{2} \\ \hline
			D & \frac{\sqrt 3}{2},\frac{1}{2} & 1,\frac{1}{2} & \frac{3}{2} \\ \hline
			E & -\frac{\sqrt 3}{2},-\frac{1}{2} & 1,\frac{1}{2} & \frac{3}{2} \\ \hline
			F & \frac{\sqrt 3}{2},-\frac{1}{2} & 1,\frac{1}{2} & \frac{3}{2} \\
		\hline
		\end{tabular}
	\end{minipage}
	\begin{minipage}{0.39\textwidth} \centering
		\begin{tikzpicture}[>=latex']
		\tikzaxes{-2}{2}{-2}{2}
		\draw[thick,black!50]
			(0,0) circle (1)
			(-2,.5) -- (2,.5)
			(-2,-.5) -- (2,-.5);
		
		\fill[pattern=north east lines] (-2,.5) -- ({cos(150)},.5) arc[x radius=1cm,y radius=1cm,start angle=150,end angle=30] -- (2,.5) -- (2,2) -- (-2,2) -- cycle;
		\draw[very thick]  (-2,.5) -- ({cos(150)},.5) arc[x radius=1cm,y radius=1cm,start angle=150,end angle=30] -- (2,.5);
		
		\fill[pattern=north east lines] (-2,-.5) -- ({cos(150)},-.5) arc[x radius=1cm,y radius=1cm,start angle=210,end angle=330] -- (2,-.5) -- (2,-2) -- (-2,-2) -- cycle;
		\draw[very thick]  (-2,-.5) -- ({cos(150)},-.5) arc[x radius=1cm,y radius=1cm,start angle=210,end angle=330] -- (2,-.5);
	
		% punti
		\tikzstyle{every node}=[shape=circle,draw=none,fill=white,inner sep=0pt]
		\fill[color=black]
			(0,1) circle (.7mm) node [above=2pt] {$A$}
			(0,-1) circle (.7mm) node [below=2pt] {$B$}
			({cos(150)},.5) circle (.7mm) node [above left=3pt] {$C$}
			({cos(150)},-.5) circle (.7mm) node [below left=3pt] {$E$}
			({cos(30)},.5) circle (.7mm) node [above right=3pt] {$D$}
			({cos(30)},-.5) circle (.7mm) node [below right=3pt] {$F$};
		\fill[color=black!50]	
			(0,.5) circle (.6mm)
			(0,-.5) circle (.6mm)
			(1,0) circle (.6mm)
			(-1,0) circle (.6mm);	
		\end{tikzpicture}
	\end{minipage}
\end{center}
\end{esempio}