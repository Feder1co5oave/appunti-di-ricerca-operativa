\chapter{Programmazione lineare}
\addtothm{Programmazione lineare}

\section{Geometria}

\begin{definizione}[Insieme convesso]
Un insieme $C$ si dice convesso se, presi due punti qualsiasi $c_1, c_2 \in C$, si ha che tutto il segmento che congiunge $c_1$ e $c_2$ è contenuto in $C$:
$$ \lambda c_1 + (1 - \lambda) c_2 \in C,\ \forall \lambda \in [0,1] $$
\end{definizione}

\begin{definizione}[Combinazione convessa]
Una combinazione convessa di un numero finito di vettori $x_1,\ldots,x_m \in \R^n$ è un vettore $v \in \R^n$ tale che
$$ v = \sum_{i=1}^m \lambda_i x_i,\quad 0 \leqslant \lambda_i \leqslant 1, \quad \sum_{i=1}^m \lambda_i = 1 $$
\end{definizione}

L'involucro convesso di un insieme di vettori $x_i$ è l'insieme di tutte le possibili combinazioni convesse degli $x_i$. Esso è il più piccolo insieme convesso che contiene tutti i punti $x_i$.

Chiamiamo \emph{propria} una combinazione convessa tale che $0 < \lambda_i < 1,\ \forall i$ (ovvero $v$ non sta sulla frontiera dell'involucro convesso).

\begin{definizione}[Cono]
Chiamiamo cono un sottoinsieme $K$ di $\R^n$ tale che per ogni punto $x \in K$ si ha che $\lambda x \in K,\ \forall \lambda \geqslant 0$.
\end{definizione}

\begin{definizione}[Combinazione conica]
Una combinazione conica di un numero finito di vettori $x_1,\ldots,x_m \in \R^n$ è un vettore $v \in \R^n$ tale che
$$ v = \sum_{i=1}^m \lambda_i x_i, \quad \lambda_i \geqslant 0,\ \forall i $$
\end{definizione}
Analogamente a sopra, l'involucro conico di un insieme di vettori $x_i$ è l'insieme di tutte le possibili combinazioni coniche degli $x_i$. Ricordare che l'\emph{involucro conico} è il più piccolo cono convesso che contiene tutti i punti $x_i$, mentre non tutti i coni sono insiemi convessi.

Una combinazione conica si chiama \emph{propria} se tutti i suoi coefficienti $\lambda_i > 0$.

\begin{definizione}[Poliedro]
Un poliedro $P$ è un'intersezione di un insieme finito di semispazi chiusi di $\R^n$ e può essere espresso nella forma
$$ P = \{ x\in \R^m : Ax \leqslant b\}, \quad A \in \R^{m \times n},\ b \in \R^m $$
\end{definizione}

\begin{definizione}[Vertice]
Un vertice di un poliedro è un punto che non può essere espresso come combinazione convessa propria di altri punti del poliedro.
\end{definizione}
Indichiamo con $\vert P$ l'insieme dei vertici di un poliedro $P$.

\begin{definizione}[Direzione di recessione] \label{def:direzionerecessione}
Una direzione di recessione per un poliedro $P$ è un vettore $d$ tale che
$$ x + \lambda d \in P, \quad \forall x \in P,\ \forall \lambda \geqslant 0 $$
\end{definizione}
L'insieme delle direzioni di recessione di un poliedro $P = \{Ax \leqslant b\}$ si può esprimere come
$$ \rec P = \{ x \in \R^n : Ax \leqslant 0\}$$
Infatti, se $x \in P$ e $d \in \rec P$, ovvero $A x \leqslant b,\ Ad \leqslant 0$, allora anche $A(x + \lambda d) = A x + \lambda A d \leqslant b$.

Un poliedro è limitato se e solo se non ha direzioni di recessione (ovvero, solo il vettore nullo). Inoltre, $\rec P$ è sia poliedro sia cono, cioè è un cono poliedrico.

\begin{definizione}[Direzione di linealità]
Chiamiamo direzione di linealità un vettore $d$ tale che $(d \in \rec P) \wedge (-d \in \rec P)$, cioè
$$ x + \lambda d \in P, \quad \forall x \in P,\ \forall \lambda \in \R $$
\end{definizione}
L'insieme delle direzioni di linealità di un poliedro si può rappresentare come $\lineal P = \{x\in \R^n : Ax = 0\}$. Inoltre, un poliedro $P$ non vuoto possiede almeno un vertice se e solo se $\lineal P = \{0\}$.

\begin{teorema}[Rappresentazione dei poliedri]
Dato un poliedro $P$, esistono un insieme finito $V \subseteq P$ e un insieme finito $E \subset \R^n$ tali che
$$ P = \conv V + \cone E $$
\end{teorema}
Seguono alcune dirette conseguenze di questo teorema:
\begin{teorema}
Dato un poliedro $P$, esiste un insieme finito $V \subseteq P$ tale che $P = \conv V + \rec P$
\end{teorema}
\begin{teorema}
Dato un poliedro $P$ tale che $\lineal P = \{0\}$ (cioè ha almeno un vertice), allora $P = \conv(\vert P) + \rec P$
\end{teorema}

\begin{teorema}
Se un poliedro ha i vincoli di positività sulle variabili ($x \geqslant 0$) allora ha vertici.
\end{teorema}

Volendo riassumere, dato un poliedro $P = \conv V + \cone E$:
\begin{itemize}
\item $\displaystyle P$ limitato $\Longleftrightarrow \ \rec P = \{0\} \ \Longleftrightarrow \ E = \{0\}$
\item $\displaystyle P$ ha vertici $\Longleftrightarrow \ \lineal P = \{0\}$
\item $|\vert P| \leqslant |V|$
\item $m \geqslant$ numero di lati di $P$
\end{itemize}

\section{Problema primale standard}
Si dice in forma primale standard un problema di programmazione lineare del tipo
\begin{equation} \label{eq:primale} \tag{$\P$}
\left\{ \begin{array}{l}
	\max\ cx \\
	Ax \leqslant b
\end{array} \right.
\end{equation}

Di seguito indicheremo con la dicitura $v(\P)$ il valore ottimo di un problema \eqref{eq:primale}.

\begin{teorema}[Caratterizzazione delle soluzioni ottime]
Dato un problema di PL in forma primale standard, valgono le seguenti affermazioni:
\begin{enumerate}
\item Se $c \neq 0$, allora ogni soluzione ottima di \eqref{eq:primale} è sulla frontiera del poliedro
\item \eqref{eq:primale} possiede zero, una o infinite soluzioni ottime di valore finito
\item Le soluzioni ottime locali di \eqref{eq:primale} sono anche ottime globali
\end{enumerate}
\end{teorema}

\begin{teorema}[Fondamentale della PL] \label{teo:fondamentalepl}
Preso il problema \eqref{eq:primale} il cui poliedro è rappresentato come $P = \conv \{v_1,\ldots,v_m\} + \cone \{e_1,\ldots,e_p\}$, allora è vera una sola delle seguenti affermazioni:
\begin{enumerate}
\item $P = \emptyset$
\item \eqref{eq:primale} ha valore ottimo finito ed esiste $k$ tale che $v_k$ è una soluzione ottima
\item esiste $h$ tale che $c e_h > 0$ e quindi $v(\P) = +\infty$
\end{enumerate}
\end{teorema}

\section{Dualità}
Al problema \eqref{eq:primale} associamo il problema duale standard, nella forma
\begin{equation} \label{eq:duale} \tag{$\D$}
\left\{ \begin{array}{l}
\min\ by \\
\trasp A y = c \\
y \geqslant 0
\end{array} \right.
\end{equation}
Si può dare un'interpretazione geometrica al problema duale, notando che $\trasp A y = \sum_i y_i A_i$, con $y_i \geqslant 0$, è una qualsiasi combinazione conica dei vettori che costituiscono le righe della matrice $A$. Quindi \eqref{eq:duale} ha soluzione se e solo se $c$ appartiene all'involucro conico generato dalle righe di $A$ e la soluzione $y$ è appunto costituita dai coefficienti della combinazione conica.

Ricordare che qualsiasi problema di PL può essere portato in forma standard primale o duale, tramite manipolazioni delle (dis)equazioni. Inoltre, di ogni problema di PL può essere costruito il relativo duale usando alcune regole (qui tralasciate). Tuttavia, per ottenere il duale di un problema in forma non standard, risulta più semplice portarlo in forma standard primale o duale, a seconda di quale viene più comodo, e poi scrivere il duale della forma standard, che per \eqref{eq:primale} è \eqref{eq:duale} e per \eqref{eq:duale} è \eqref{eq:primale}. Infatti, \emph{il duale del duale è il primale}.

Enunciamo ora un teorema utile a dimostrare alcuni di quelli a venire:
\begin{teorema}[Farkas]
Data una matrice $A \in \R^{m \times n}$ ed un vettore $c \in \R^n$, i due seguenti sistemi sono in alternativa, cioè il primo ammette soluzioni se e solo se il secondo è impossibile.
$$
\sistema{
	\trasp A y = c \\
	y \geqslant 0
}
\qquad
\sistema{
	A d \leqslant 0 \\
	c d > 0
}
$$
\end{teorema}
\`E possibile dare un'interessante interpretazione al teorema di Farkas: il primo sistema è un problema duale standard, mentre le soluzioni $d$ del secondo sistema sono tutte le direzioni di recessione (vedi \thref{def:direzionerecessione}) del poliedro del primale che hanno prodotto scalare positivo con il vettore di guadagno $c$, ovvero quelle spostandosi lungo le quali (in verso positivo) la funzione obiettivo cresce. Infatti, se $cd > 0$, allora $c (x + \lambda d) = cx + \lambda cd > cx$. Quindi, il teorema afferma che \eqref{eq:duale} è vuoto se e solo se esiste una direzione di recessione di $P$ che è anche di crescita per la funzione obiettivo, cioè se l'ottimo di \eqref{eq:primale} è $+\infty$, e viceversa: se il poliedro $D$ di \eqref{eq:duale} è non vuoto, allora \eqref{eq:primale} ha soluzione ottima di valore finito (se $P \neq \emptyset$).

\begin{teorema}[Dualità debole]
Se i poliedri $P$ e $D$ sono non vuoti, allora
$$ cx \leqslant by \quad \forall x \in P, \forall y \in D $$
\end{teorema}
\begin{dimostrazione}
Semplicemente applicando delle sostituzioni\footnote{Per convenzione fino ad ora abbiamo indicato un prodotto scalare tra vettori semplicemente come $c x$, ma formalmente andrebbe scritto come prodotto righe per colonne in questo modo: $\trasp c x$. Inoltre, ricordare la regola del trasposto di un prodotto: $\trasp{(AB)} = \trasp B \trasp A$.}, si ha $ c x = \trasp c x = \trasp y A x \leqslant \trasp y b = by $ (presupposto che $y \geqslant 0$).
\end{dimostrazione}

\begin{teorema}[Dualità forte] \label{teo:dualitàforte}
Se i poliedri $P$ e $D$ sono non vuoti, allora
$$ -\infty < v(\P) = v(\D) < +\infty $$
\end{teorema}

Ricapitolando:
\begin{center}\begin{tabular}{| >{$}c<{$} | >{$}c<{$} | >{$}c<{$} |}
\hline
& D \neq \emptyset & D = \emptyset \\
\hline
P \neq \emptyset & v(\P) = v(\D) \in \R & v(\P) = +\infty \\
\hline
P = \emptyset & v(\D) = -\infty & $(caso patologico)$ \\
\hline
\end{tabular}\end{center}

Spesso si dice che $v(\P) = v(\D)$ se almeno uno tra i poliedri $P$ e $D$ è non vuoto, poiché si assume per convenzione che
$\displaystyle\max_{P = \emptyset}\ c x = -\infty$ e $\displaystyle\min_{D = \emptyset}\ by = +\infty$.

\begin{teorema}[Scarti complementari] \label{teo:scarticomplementari}
Siano $\bar x$ e $\bar y$ ammissibili rispettivamente per \eqref{eq:primale} e \eqref{eq:duale}. Allora
$$ \bar y (b - A \bar x) = 0 \quad \Longleftrightarrow \quad \bar x\ \mathrm{e}\ \bar y\ \mathrm{ottime} $$
\end{teorema}
\begin{dimostrazione}
Usando direttamente il \thref{teo:dualitàforte}, $\trasp c \bar x = \trasp{\bar y} A \bar x = \trasp{\bar y} b$, ovvero $\trasp{\bar y} (b - A \bar x) = 0$. Notare che l'ultima equazione può essere espansa in $(\bar y_i = 0) \vee (b_i = A_i \bar x),\ \forall i$.
\end{dimostrazione}

\section{Algebra}

\begin{definizione}[Base]
Ricordando che $A \in \R^{m \times n}$, chiamiamo base un insieme $B \subseteq \{1,\ldots,m\}$ di $n$ elementi tale che la matrice estratta $A_B$ composta dalle righe di $A$ che hanno un indice contenuto in $B$ abbia determinante diverso da zero. ($\det A_B \neq 0$)
\end{definizione}
Per convenzione, indichiamo con $N$ l'insieme complementare di $B$ rispetto a $\{1,\ldots,m\}$.

Data una base, le soluzioni di base $\bar x$ e $\bar y$ si ottengono risolvendo le seguenti equazioni:
\begin{equation} \label{eq:soluzionibase}
A_B \bar x = b_B \qquad \sistema{ \trasp A_B \bar y_B = c \\ \bar y_N = 0}
\end{equation}
e quindi
\begin{equation} \label{eq:soluzionibaseesplicite}
\bar x = A_B^{-1} b_B \qquad \bar y_B = \trasp c A_B^{-1} = \trasp{(A_B^{-1})} c,\quad \bar y_N = 0
\end{equation}

\subsection{Soluzioni di base ammissibili, degeneri e ottime} \label{par:pl-algebra-solbase}

Data una coppia di problemi \eqref{eq:primale} e \eqref{eq:duale} e una base $B$, si possono calcolare le due soluzioni di base $\bar x,\ \bar y$. Una soluzione di base può essere ammissibile e/o degenere. Come si vedrà più avanti, due soluzioni di base entrambe ammissibili sono anche entrambe ottime.

\begin{center} \begin{tabular}{|c|c|c|}
\hline
& sol. primale $\bar x$ & sol. duale $\bar y$ \\
\hline
ammissibile se & $A_N \bar x \leqslant b_N$ & $\bar y_B \geqslant 0$ \\
\hline
\textsc{non} degenere se & $A_N \bar x < b_N$ & $\bar y_B > 0$ \\
\hline
\end{tabular} \end{center}

Due soluzioni di base $\bar x,\ \bar y$ si dicono complementari perché sono in scarti complementari (vale il \thref{teo:scarticomplementari}): è facilmente osservabile dalle equazioni \eqref{eq:soluzionibase}.

Una soluzione è degenere se è generata da più di una base. Un vincolo si dice \emph{superfluo} se le basi che lo contengono generano soltanto soluzioni di base non ammissibili. Invece, un vincolo \emph{ridondante} è un vincono non superfluo tale che le basi che lo contengono generano solo soluzioni non ammissibili o ammissibili e degeneri.

\begin{center}
	\begin{minipage}{0.69\textwidth} \centering
		\begin{tikzpicture}[>=latex']
		% Griglia e assi
		\tikzaxes{-3}{3}{-3}{3}
		
		% Pattern
		\fill[pattern=north east lines,pattern color=red] (-3,1.9) -- (3,1.9) -- (3,2) -- (-3,2) -- cycle;
		\fill[pattern=north east lines,pattern color=blue] (0,3) -- (-.12,3) -- (2.88,-3) -- (3,-3) -- cycle;
		\fill[pattern=north east lines] (-3,1) -- (0,1) -- (2,-1) -- (2,-3) -- (-3,-3) -- cycle;
		
		% Vincoli
		\draw[very thick]
			(-2,3) -- (3,-2)
			(2,3) -- (2,-3)
			(-3,1) -- (3,1);
		\draw[very thick,color=red] (-3,2) -- (3,2) node [pos=0.2,sloped,anchor=south,fill=white,fill opacity=0.8,text opacity=1,yshift=1pt] {superfluo};
		\draw[very thick,color=blue] (0,3) -- (3,-3) node [pos=0.4,sloped,anchor=south,fill=white,fill opacity=0.8,text opacity = 1] {ridondante};
		
		% Soluzioni
		\fill[color=black]
			(2,1) circle [radius=.7mm]
			(2,-1) circle [radius=.7mm]
			(0,1) circle [radius=.7mm];
		\draw[anchor=base west] 
			(2,1) node [yshift=0.3em] {non ammissibile}
			(2,-1) node [yshift=0.3em] {degenere}
			(0,1) node [anchor=north east,fill=white,fill opacity=0.8,text opacity = 1,inner sep=2pt,shift={(-.05,-.05)}] {ammissibile};
		\end{tikzpicture}
	\end{minipage}
	\begin{minipage}{0.3\textwidth} \centering
		$$ \sistema{
			x_1 \leqslant 2 \\
			x_2 \leqslant 1 \\
			x_1 + x_2 \leqslant 1 \\
			\color{red} x_2 \leqslant 2 \\
			\color{blue} 2 x_1 + x_2 \leqslant 3
		} $$
	\end{minipage}
\end{center}

\begin{teorema}[Condizioni suff. di ottimalità] \label{teo:condizionisuffottimalità}
Date due soluzioni di base complementari $\bar x,\ \bar y$ rispettivamente per \eqref{eq:primale} e \eqref{eq:duale}, allora
$$ \bar x,\ \bar y\ \mathrm{ammissibili} \quad \Longrightarrow \quad \bar x,\ \bar y\ \mathrm{ottime} $$
\end{teorema}

\begin{teorema}[Relazione tra vertici e soluzioni di base] \label{teo:vertice}
Un punto $\bar x$ è un vertice del poliedro $P$ se e solo se $\bar x$ è una soluzione di base ammissibile.
\end{teorema}

Per un problema \eqref{eq:primale} a $n$ incognite e $m$ disequazioni esistono $\displaystyle \left(n \atop m\right)$ possibili basi.

\section{Algoritmo del simplesso}

L'algoritmo del simplesso consente di risolvere problemi di programmazione lineare (purché ammettano almeno una base), passando da una base ammissibile all'altra e terminando, eventualmente, sulla base ottima.

Prendiamo per esempio il simplesso primale: cerchiamo innanzitutto una base che generi una soluzione di base primale $\bar x$ ammissibile. Se la sua soluzione di base complementare $\bar y \geqslant 0$, allora l'algoritmo termina perché ha raggiunto una base ottimale ($c\bar x = b\bar y$), altrimenti effettua un cambio di base $B' = B \setminus \{h\} \cup \{k\}$ e ripete il procedimento. Se non è possibile effettuare il cambio di base significa che il poliedro è illimitato e $v(\P) = +\infty$.

\subsection{Algoritmo del simplesso primale}

Per avviare l'algoritmo è necessario disporre di una base di partenza che generi una soluzione di base primale ammissibile. In seguito si vedrà come risolvere questo problema usando un ``problema ausiliario''. Chiamiamo $B$ la base di partenza. Allora:
\begin{enumerate}
\item Calcolare le soluzioni di base $\bar x$ e $\bar y$ utilizzando la \eqref{eq:soluzionibaseesplicite}.
\item Se $\bar y_B \geqslant 0$ allora \textsc{stop $\Longrightarrow \: B$ è una base ottima}. Altrimenti:
\item Selezionare l'indice uscente $h = \min\ \{i \in B : \bar y_i < 0\}$ (regola anticiclo di Bland) e calcolare il vettore $W^h = \left(-A_B^{-1} \right)^h$ ($h$-esima colonna).
\item Calcolare i prodotti $A_i W^h,\ \forall i \in N$.
\item Se tutti gli $A_i W^h \leqslant 0$ allora \textsc{stop $\Longrightarrow \: v(\P) = +\infty,\ W^h \in \rec P$}. Altrimenti:
\item Per tutti gli $A_i W^h > 0$ calcolare i rapporti $\displaystyle r_i = \frac{b_i - A_i \bar x}{A_i W^h}$ e selezionare l'indice entrante $k = \min\ \{\: j : r_j = \min\ r_i\:\}$ (regola anticiclo di Bland).
\item Effettuare il cambio di base $B \gets B \setminus \{h\} \cup \{k\}$ e ritornare al punto 1.
\end{enumerate}

\begin{esempio}
Applichiamo l'algoritmo al seguente problema di PL in forma primale standard:
\begin{center}
%\includegraphics[width=\textwidth]{../algoritmi/lp/result}
\hspace*{\stretch{1}}
\begin{minipage}{0.5\textwidth}
	\begin{tikzpicture}[>=latex']
	\tikzaxes{-2}{5}{-1}{5}
	\tikzstyle{every node}=[midway,draw,thin,shape=circle,inner sep=.5pt,outer sep=2pt,fill=white]
	\filldraw [pattern=north west lines,very thick] (-1,0) --
		(4,0) node [anchor=north] {2} --
		(4,2) node [anchor=west] {3} --
		(2,4) node [anchor=south west] {4} --
		(0,3) node [anchor=south east] {5} --
		(-1,0) node [anchor=east] {1} -- cycle;
	\draw[white,ultra thick,shorten <=2pt,shorten >=-2pt] (0,0) -- (3,4);
	\draw[->,blue,shorten <=1pt] (0,0) -- (3,4) node [anchor=south west,draw=none,pos=1] {$c$};	
	\end{tikzpicture}
\end{minipage}
\hspace*{\stretch{1}}
\begin{minipage}{0.4\textwidth}
	\begin{math} \displaystyle \sistema{
		\color{blue} \max\ 3 x_1 + 4 x_2 \\
		\!\!\!\! \begin{array}{r@{x_1\:}r@{x_2 \leqslant\;}l}
		-3 & +1 & 3 \\
		0 & -1 & 0 \\
		1 & +0 & 4 \\
		1 & +1 & 6 \\
		-1 & +2 & 6
		\end{array}
	}\end{math}
\end{minipage}
\end{center}

$$
A = \left[ \begin{array}{cc} -3 & 1 \\ 0 & -1 \\ 1 & 0 \\ 1 & 1 \\ -1 & 2 \end{array} \right] \qquad
c = \left[ \begin{array}{c} 3 \\ 4 \end{array} \right] \qquad
b = \left[ \begin{array}{c} 3 \\ 0 \\ 4 \\ 6 \\ 6 \end{array} \right] \qquad
B = \{1,2\}\ \mathrm{(base\ di\ partenza)}
$$
\begin{center} \makebox[\textwidth]{%
\begin{minipage}{0.6\textwidth}
	\begin{enumerate}
	\setlength{\parskip}{0pt}
	\item $\bar x = (-1,0) \qquad \bar y = (-1,-5,0,0,0)$
	\resetcounter[3]
	\item $h = 1 \qquad W^h = (\nicefrac{1}{3},0)$
	\item $A_3 W^h = \nicefrac{1}{3} \quad A_4 W^h = \nicefrac{1}{3} \quad \cancel{A_5 W^h = -\nicefrac{1}{3}} \leqslant 0$
	\resetcounter[6]
	\item $r_3 = 15 \quad r_4 = 21 \qquad k = 3$
	\item $B \gets \{2,3\}$
	\hrule \resetcounter
	\item $\bar x = (4,0) \qquad \bar y = (0,-4,3,0,0)$
	\resetcounter[3]
	\item $h = 2 \qquad W^h = (0,1)$
	\item $A_1 W^h = 1 \quad A_4 W^h = 1 \quad A_5 W^h = 2$
	\resetcounter[6]
	\item $r_1 = 15 \quad r_4 = 2 \quad r_5 = 5 \qquad k = 4$
	\item $B \gets \{3,4\}$
	\hrule \resetcounter
	\item $\bar x = (4,2) \qquad \bar y = (0,0,-1,4,0)$
	\resetcounter[3]
	\item $h = 3 \qquad W^h = (-1,1)$
	\item $A_1 W^h = 4 \quad \cancel{A_2 W^h = -1} \leqslant 0 \quad A_5 W^h = 3$
	\resetcounter[6]
	\item $r_1 = \nicefrac{13}{4} \quad r_5 = 2 \qquad k = 5$
	\item $B \gets \{4,5\}$
	\hrule \resetcounter
	\item $\bar x = (2,4) \qquad \bar y = (0,0,0,\nicefrac{10}{3},\nicefrac{1}{3})$
	\item $\bar y \geqslant 0 \Longrightarrow\ $ \textsc{stop} (base ottima raggiunta)
	\end{enumerate}
\end{minipage}
\hspace*{1cm}
\begin{minipage}{0.5\textwidth}
	\begin{tikzpicture}[>=latex']
		\tikzaxes{-2}{5}{-1}{5}
		\tikzstyle{every node}=[midway,draw,thin,shape=circle,inner sep=.5pt,outer sep=2pt,fill=white]
		\fill [pattern=north west lines]
			(-1,0) --
			(4,0) node [anchor=north] {2} --
			(4,2) node [anchor=west] {3} --
			(2,4) node [anchor=south west] {4} --
			(0,3) node [anchor=south east] {5} --
			(-1,0) node [anchor=east] {1} -- cycle;
		\draw[very thick] (-1,0) -- (0,3) -- (2,4);
		\draw[white,ultra thick,shorten <=2pt,shorten >=-2pt] (0,0) -- (3,4);
		\draw[->,blue,shorten <=1pt] (0,0) -- (3,4) node [anchor=south west,draw=none,pos=1] {$c$};
		\draw[->,red,very thick] (-1,0) -- (4,0) -- (4,2) -- (2,4) node [pos=1,outer sep=5pt,anchor=south,fill=white,shape=rectangle,draw=none] {sol. ottima};
		\fill[red]
			(-1,0) circle[radius=.7mm]
			(4,0) circle[radius=.7mm]
			(4,2) circle[radius=.7mm]
			(2,4) circle[radius=.7mm];
	\end{tikzpicture}
\end{minipage}
} \end{center}
\end{esempio}

\begin{teorema}[Correttezza del simplesso primale]
L'algoritmo del simplesso primale risolve il problema di PL in forma primale standard \eqref{eq:primale} in un numero finito di passi.
\end{teorema}
\begin{dimostrazione}
Ad una qualsiasi iterazione, se le soluzioni di base primale e duale sono ammissibili, l'algoritmo ha raggiunto l'ottimo per il \thref{teo:condizionisuffottimalità}. Altrimenti, si consideri la semiretta parametrica uscente dal vertice $\bar x$ in direzione $W^h$: $x(\lambda) = \bar x + \lambda W^h,\ \lambda \geqslant 0$. La funzione obiettivo sulla semiretta vale $cx(\lambda) = c\bar x + \lambda c W^h$. Vogliamo trovare un indice $h \in B$ in modo che essa cresca lungo $x(\lambda)$, quindi $cx(\lambda) > c\bar x$, cioè $c W^h > 0$.

Poiché $\trasp c A_B^{-1} = \bar y_B$, ovvero $\bar y_i = -c W^i,\ \forall i \in B$, allora l'ultima equazione è verificata per ogni $i \in B$ tale che $\bar y_i < 0$. Scegliere l'indice uscente $h$ con la regola anticiclo di Bland garantisce che si visiti al più una volta la stessa base nel caso in cui esistano basi ammissibili degeneri (se non esistono il problema non si pone).

Notare che la semiretta $x(\lambda)$ punta sempre all'interno del poliedro e percorrendola ci si allontana dal vincolo di indice $h$. Analizziamo quali punti della semiretta appartengono al poliedro: $Ax(\lambda) \leqslant b$. Le disequazioni sono sicuramente verificate per i vincoli in base ($i \in B$): 
$$ A_i W^h = (A_B W)_{ih} = (-I)_{ih} = \sistema[l@{\quad}l]{ -1 & i = h \\ 0 & i \neq h }
\quad \Longrightarrow \quad
\underbrace{A_i \bar x}_{=b_i} + \underbrace{\lambda A_i W^h}_{\leqslant 0} \leqslant b_i $$
Per i vincoli non in base, invece,
$$ A_N x(\lambda) \leqslant b_N \quad \Longleftrightarrow \quad \sistema[l@{\quad}l]{
	\lambda \geqslant 0													& \mathrm{se\ } A_i W^h \leqslant 0 \\
	\displaystyle \lambda \leqslant \frac{b_i - A_i \bar x}{A_i W^h}	& \mathrm{se\ } A_i W^h > 0
} \quad \forall i \in N
$$
Nel primo caso l'$i$-esimo vincolo non interseca la semiretta, mentre nel secondo sì. Nel caso in cui tutti gli $A_i W^h \leqslant 0$ con $i \in N$, avremmo che la semiretta è interamente contenuta nel poliedro, cioè $W^h$ è una direzione di recessione (e di crescita per la funzione obiettivo) e quindi l'algoritmo si ferma perché $v(\P) = +\infty$. Se invece c'è qualche $A_i W^h > 0$, il massimo $\lambda$ per cui $x(\lambda) \in P$ è quindi
$$\bar \lambda = \min\ \Big\{ \frac{b_i - A_i \bar x}{A_i W^h} : i \in N,\ A_i W^h > 0 \Big\} $$
Scegliamo l'indice entrante $k$ tale che
$$ \bar\lambda = \frac{b_k - A_k \bar x}{A_k W^h},\ A_k W^h > 0 $$
Il nuovo punto ottenuto $x(\bar\lambda)$ è un vertice del poliedro, soluzione di base generata da $B' = B \setminus \{h\} \cup \{k\}$:
$$ A_k x(\bar\lambda) = b_k \quad \Longleftrightarrow \quad A_k x(\bar\lambda) = A_k \bar x + \bar\lambda A_k W^h = A_k \bar x + \frac{b_k -A_k \bar x}{\cancel{A_k W^h}} \cancel{A_k W^h} = b_k $$
$$ A_j x(\bar\lambda) = b_j,\ \forall j \in B \setminus \{h\} \quad \Longleftrightarrow \quad
A_j \bar x + \bar\lambda \underbrace{A_j W^h}_{=0} = b_j $$

$\bar\lambda = 0$ rappresenta un interessante caso particolare: accade quando la base corrente è degenere, poiché esiste un $i \in N$ tale che $b_i - A_i \bar x = 0$ e che verrà quindi scelto come indice entrante $k$. In questo caso la nuova soluzione di base $x(\bar\lambda) = x(0) = \bar x$.

Rimane da dimostrare che il cambio di indici provochi un cambio di base valido, ovvero $\det A_{B'} \neq 0$. Geometricamente, il vettore $W^h$ è parallelo a tutti i vincoli in base tranne quello uscente e, per come scegliamo il vincolo entrante ($A_k W^h > 0$), si ottiene che esso è linearmente indipendente agli altri, cioè la loro intersezione è un vertice, quindi $A_{B'}$ è invertibile.
\end{dimostrazione}

\subsection{Osservazioni}

Porre sempre attenzione a non generalizzare troppo facilmente fenomeni che hanno luogo soltanto nel piano cartesiano ($\R^2$). Un esempio importante è il comportamento dell'algoritmo in presenza di vertici degeneri, cioè generati da più di una base. Se un vertice $\bar x$ è generato da $r$ basi $B_1,\ldots,B_r$, ad esso corrispondono $r$ soluzioni duali complementari $\bar y_1,\ldots,\bar y_r$. Alcune di queste basi possono essere ammissibili, altre no.

Prendiamo come esempio un poliedro in $\R^3$: una piramide a base quadrata di lato 1, di altezza $\nicefrac{1}{2}$ e con il vertice posizionato sopra il baricentro della base. Prendiamo come vettore $c = (1,0,2)$ e applichiamo l'algoritmo del simplesso, usando la base di partenza $B = \{1,2,5\}$:
\begin{center}
\hspace*{\stretch{1}}
\begin{minipage}{0.6\textwidth}
	\begin{enumerate}
	\setlength{\parskip}{0pt}
	
	\item $\bar x = (0,1,0) \qquad \bar y = (-3,-1,0,0,0)$
	\resetcounter[3]
	\item $h = 1 \qquad W^h = (1,-1,1)$
	\item $A_3 W^h = 2 \quad A_4 W^h = 2$
	\resetcounter[6]
	\item $r_3 = \nicefrac{1}{2} \quad r_4 = \nicefrac{1}{2} \qquad k = 3$
	\item $B \gets \{2,3,5\}$
	\hrule \resetcounter
	\item $\bar x = (\nicefrac{1}{2},\nicefrac{1}{2},\nicefrac{1}{2}) \qquad \bar y = (0,-1,\nicefrac{3}{2},0,\nicefrac{3}{2})$
	\resetcounter[3]
	\item $h = 2 \qquad W^h = (1,0,0)$
	\item $\cancel{A_1 W^h = 0} \leqslant 0 \quad A_4 W^h = 1$
	\resetcounter[6]
	\item $r_4 = 0 \qquad k = 4$
	\item $B \gets \{3,4,5\}$
	\hrule \resetcounter
	\item $\bar x = (\nicefrac{1}{2},\nicefrac{1}{2},\nicefrac{1}{2}) \qquad \bar y = (0,0,\nicefrac{1}{2},1,\nicefrac{1}{2})$
	\item $\bar y \geqslant 0\ \Longrightarrow\ $ \textsc{stop} (base ottima raggiunta)
	\end{enumerate}
\end{minipage}
\hspace*{2cm}
\begin{minipage}{0.2\textwidth}
	\begin{math} \displaystyle
	\sistema{
		\max\ x_1 + 2 x_3 \\
		x_3 \geqslant 0 \\
		x_3 - x_1 \leqslant 0 \\
		x_3 - x_2 \leqslant 0 \\
		x_3 + x_1 \leqslant 1 \\
		x_3 + x_2 \leqslant 1
	}
	\end{math}
\end{minipage}
\hspace*{\stretch{1}}
\end{center}

Come si vede dalla traccia dell'esecuzione, il vertice ottimo è degenere e l'algoritmo visita due basi distinte che lo generano. La prima, $\{2,3,5\}$, genera però una soluzione duale non ammissibile e quindi non è ottima.

\subsection{Algoritmo del simplesso duale}

L'algoritmo duale procede in modo analogo a quello primale, solo che la base di partenza deve generare una soluzione di base \emph{duale} ammissibile.

\begin{enumerate}
\item Calcolare le soluzioni di base $\bar x$ e $\bar y$ utilizzando la \eqref{eq:soluzionibaseesplicite}.
\item Se $A_N \bar x \leqslant b_N$ allora \textsc{stop $\Longrightarrow\ B$ è una base ottima}. Altrimenti:
\item Selezionare l'indice entrante $k = \min\ \{i \in N : A_i \bar x > b_i\}$ (regola anticiclo di bland).
\item Calcolare i prodotti $A_k W^i,\ \forall i \in B$, con $W = -A_B^{-1}$.
\item Se tutti gli $A_k W^i \geqslant 0$ allora \textsc{stop $\Longrightarrow\ v(\D) = -\infty$}. Altrimenti:
\item Per tutti gli $A_k W^i < 0$ calcolare i rapporti $\displaystyle r_i = \frac{-\bar y_i}{A_k W^i}$ e selezionare l'indice uscente $h = \min\ \{ j : r_j = \min\ r_i \}$ (regola anticiclo di Bland).
\item Effettuare il cambio di base $B \gets B \cup \{k\} \setminus \{h\}$ e ritornare al punto 1.
\end{enumerate}

\subsection{Invertire una matrice}

Nell'esecuzione dell'algoritmo del simplesso una delle operazioni più complesse da completare è il calcolo dell'inversa della matrice in base: $A_B^{-1}$. Se dobbiamo farlo a mano e su matrici di dimensione ridotta ($2 \div 3$), conviene sfruttare la formula seguente:
\begin{equation} \label{eq:matriceinversa}
M^{-1} = \{n_{ij}\}
\qquad
n_{ij} = (-1)^{i + j} \frac{\det M_{ji}}{\det M}
\end{equation}
dove $M_{ji}$ è la matrice $M$ privata della $j$-esima riga e dell'$i$-esima colonna. Se $M \in \R^{2 \times 2}$, la \eqref{eq:matriceinversa} si riduce a
$$
M = \matrice{cc}{
	a & b \\ c & d
},
\qquad
D = \det M = ad - bc,
\qquad
M^{-1} = \matrice{cc}{
	\nicefrac{d}{D} & \nicefrac{-b}{D} \\
	\nicefrac{-c}{D} & \nicefrac{a}{D}
}
$$

\begin{esempio}
$$ M = \matrice{ccc}{
	4 & 7 \\
	1 & 0
},
\quad
\det M = -7,
\quad
M^{-1} = \matrice{cc}{
	\nicefrac{0}{-7} & \nicefrac{-7}{-7} \\
	\nicefrac{-1}{-7} & \nicefrac{4}{-7}
} = \matrice{cc}{
	0 & 1 \\
	\nicefrac{1}{7} & -\nicefrac{4}{7}
} $$
\end{esempio}

\begin{esempio}
$$ M = \matrice{ccc}{
	0 & 2 & 1 \\
	0 & 3 & 1 \\
	1 & 0 & 4
},
\quad
\det M = 2 \cdot 1 \cdot 1 - 1 \cdot 3 \cdot 1 = -1
$$
$$
M^{-1} = \matrice{rrr}{
	-\left| \begin{array}{cc} 3 & 1 \\ 0 & 4 \end{array} \right| &
	 \left| \begin{array}{cc} 2 & 1 \\ 0 & 4 \end{array} \right| &
	-\left| \begin{array}{cc} 2 & 1 \\ 3 & 1 \end{array} \right| \\
	 \left| \begin{array}{cc} 0 & 1 \\ 1 & 4 \end{array} \right| &
	-\left| \begin{array}{cc} 0 & 1 \\ 1 & 4 \end{array} \right| &
	 \left| \begin{array}{cc} 0 & 1 \\ 0 & 1 \end{array} \right| \\
	-\left| \begin{array}{cc} 0 & 3 \\ 1 & 0 \end{array} \right| &
	 \left| \begin{array}{cc} 0 & 2 \\ 1 & 0 \end{array} \right| &
	-\left| \begin{array}{cc} 0 & 2 \\ 0 & 3 \end{array} \right|
} = \matrice{ccc}{
	-12 & 8 & 1 \\
	-1 & 1 & 0 \\
	3 & -2 & 0
}
$$
\end{esempio}

\section{Problema primale ausiliario}

Allo scopo di ottenere una base di partenza ammissibile per l'algoritmo del simplesso, vediamo la tecnica del problema ausiliario. Preso un problema primale standard \eqref{eq:primale} ed una sua base $B$ non ammissibile, calcoliamo la soluzione di base $\bar x$ e dividiamo i vincoli non in base tra quelli soddisfatti e quelli violati:
$$ U = \{ i \in N : A_i \bar x \leqslant b_i \} \quad V = \{ i \in N : A_i \bar x > b_i\} $$
Associamo ora il seguente problema di PL:
\begin{equation} \label{eq:primaleausiliario} \tag{$\mathcal{PA}$}
\sistema[l]{
	\max\ - \sum \varepsilon_i \\
	A_B x \leqslant b_B \\
	A_U x \leqslant b_U \\
	A_V x - \varepsilon \leqslant b_V \\
	\varepsilon \geqslant 0
}
\qquad
\sistema{
	\max\ - \sum \varepsilon_i \\
	\matrice{cc}{
		A_B & 0 \\
		A_U & 0 \\
		A_V & -I \\
		 0  & -I
	}
	\vettore{
		x \\ \varepsilon
	}
	\leqslant
	\vettore{
		b_B \\ b_U \\ b_V \\ 0
	}
}
\end{equation}
Le variabili $\varepsilon \in \R^p$, dove $p$ è uguale al numero di vincoli violati da $\bar x$ (cioè $|V|$), sono chiamate ausiliarie. L'aspetto interessante di \eqref{eq:primaleausiliario} è che possiede una base ammissibile nota:
$$ \tilde B = B \cup V
\qquad
A_{\tilde B} = \matrice{cc}{ A_B & 0 \\ A_V & -I }
\qquad
\sistema{
	\tilde x = A_B^{-1} b_B = \bar x \\
	\tilde \varepsilon = A_V \tilde x - b_V
} $$
Sarà sufficiente usare il simplesso primale per risolvere \eqref{eq:primaleausiliario} e distinguere due casi:
\begin{itemize} \setlength{\parskip}{0pt}
\item $v(\P\A) < 0 \Longleftrightarrow P = \emptyset$, \eqref{eq:primale} non ha basi ammissibili,
\item $v(\P\A) = 0 \Longleftrightarrow P \neq \emptyset$, \eqref{eq:primale} ha almeno una base ammissibile che si costruisce a partire dalla base ottima di \eqref{eq:primaleausiliario}.
\end{itemize}

\section{Problema duale ausiliario}

Preso il problema duale standard \eqref{eq:duale}, associamo il seguente problema di PL:
\begin{equation} \label{eq:dualeausiliario} \tag{$\mathcal{DA}$}
\sistema{
	\min\ \sum w_i \\
	\trasp A y + w = c \\
	y, w \geqslant 0
}
\qquad
\sistema{
	\min\ \sum w_i \\
	\matrice{cc}{ \trasp A & I } \vettore{y \\ w} = c \\
	y, w \geqslant 0
}
\end{equation}
Le $w \in \R^n$ sono chiamate variabili ausiliarie. L'aspetto interessante di \eqref{eq:dualeausiliario} è che possiede una base ammissibile nota:
$$ \tilde B = \{m+1, m+2, \ldots, m+n\}
\qquad
\trasp A_{\tilde B} = \matrice{cc}{ 0 & I }
\qquad
\sistema{
	\tilde y = 0 \\
	\tilde w = c
} $$
a condizione che $c \geqslant 0$. Se non lo fosse, è possibile moltiplicare per $-1$ le equazioni di \eqref{eq:duale} in cui $c_i < 0$.

Sarà allora sufficiente risolvere \eqref{eq:dualeausiliario} e distinguere due casi:
\begin{itemize} \setlength{\parskip}{0pt}
\item $v(\D\A) > 0 \Longleftrightarrow D = \emptyset$, \eqref{eq:duale} non ha basi ammissibili,
\item $v(\D\A) = 0 \Longleftrightarrow D \neq \emptyset$, \eqref{eq:duale} ha almeno una base ammissibile che si costruisce a partire dalla base ottima di \eqref{eq:dualeausiliario}.
\end{itemize}