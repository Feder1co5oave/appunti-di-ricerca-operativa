\section{Flusso massimo}

Consideriamo il problema di voler spedire una quantità massima di flusso da un nodo $s$ sorgente a un nodo $t$ destinazione, entrambi appartenenti a un grafo sui cui archi sussistono vincoli di capacità superiore.

\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {7} (2)
			edge node {13} (3);
\path (2)	edge node {6} (3)
			edge node {7} (4)
			edge node {2} (5);
\path (3)	edge node {5} (5)
			edge node {7} (6);
\path (4)	edge node {2} (5)
			edge node {5} (7);
\path (5)	edge node {3} (6)
			edge node {10} (7);
\path (6)	edge node {8} (7);
\end{tikzpicture} \end{center}
$$ E = \matrice{*6c *6c}{ % *12c semplicemente non vuole funzionare
%	12   13   23   24   25   35   36   45   47   56   57   67
	-1 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 \\ % 1
	 1 &  0 & -1 & -1 & -1 &  0 &  0 &  0 &  0 &  0 &  0 &  0 \\ % 2
	 0 &  1 &  1 &  0 &  0 & -1 & -1 &  0 &  0 &  0 &  0 &  0 \\ % 3
	 0 &  0 &  0 &  1 &  0 &  0 &  0 & -1 & -1 &  0 &  0 &  0 \\ % 4
	 0 &  0 &  0 &  0 &  1 &  1 &  0 &  1 &  0 & -1 & -1 &  0 \\ % 5
	 0 &  0 &  0 &  0 &  0 &  0 &  1 &  0 &  0 &  1 &  0 & -1 \\ % 6
	 0 &  0 &  0 &  0 &  0 &  0 &  0 &  0 &  1 &  0 &  1 &  1    % 7
}
$$
Il problema può essere formulato come un problema di PL in questo modo:
\begin{equation} \label{eq:mfmcpl} \tag{MF}
\sistema{
	\max\ v \\
	Ex = b \\
	0 \leqslant x \leqslant u
}
\qquad
b_i = \sistema[l @{\quad} l]{
	-v	& \mathrm{se\ } i = s \\
	v	& \mathrm{se\ } i = t \\
	0	& \mathrm{altrimenti}
}
\end{equation}
O, equivalentemente (applicando qualche sostituzione ed eliminando le equazioni superflue):
$$ \sistema{
	\displaystyle \max\ \sum_{(s,i) \in A} x_{si} \\
	E' x = 0 \\
	0 \leqslant x \leqslant u
} $$
dove $E'$ è la matrice $E$ privata delle righe relative ai nodi $s$ e $t$. Notare che
$$ \max\ \sum_{(s,i) \in A} x_{si} = \max\ \sum_{(i,t) \in A} x_{it} $$
Il \thref{teo:interezza} dell'interezza assicura ancora una volta che $x_{ij} \in \Z$ e quindi non è necessario aggiungere questo vincolo.

Può essere inoltre trasformato in un problema di flusso di costo minimo, impostando bilanci \emph{nulli} su tutti i nodi e aggiungendo un arco fittizio di costo $-1$ (cioè un arco ``di guadagno'') dalla destinazione alla sorgente, senza vincolo di capacità superiore. Tutti gli altri archi hanno costo zero.
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$},pin=90:0] {1};
\node (2) [above right=of 1,pin=135:0] {2};
\node (3) [below right=of 1,pin=-135:0] {3};
\node (5) [below right=of 2,pin=-90:0] {5};
\node (4) [above right=of 5,pin=45:0] {4};
\node (6) [below right=of 5,pin=-45:0] {6};
\node (7) [below right=of 4,label=right:{$=t$},pin=90:0] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {$(0,7)$} (2)
			edge node {$(0,13)$} (3);
\path (2)	edge node {$(0,6)$} (3)
			edge node {$(0,7)$} (4)
			edge node {$(0,2)$} (5);
\path (3)	edge node {$(0,5)$} (5)
			edge node {$(0,7)$} (6);
\path (4)	edge node {$(0,2)$} (5)
			edge node {$(0,5)$} (7);
\path (5)	edge node {$(0,3)$} (6)
			edge node {$(0,10)$} (7);
\path (6)	edge node {$(0,8)$} (7);
\path[draw,->] (7) |- +(-4.24,-3.2) node [anchor=north] {$(-1,+\infty)$} -| (1);
\end{tikzpicture} \end{center}
\begin{equation} \label{eq:mfmccomemcf}
\sistema{
	\min\ -v \\
	E_1 x + v = 0 \\
	E_2 x = 0 \\
	\quad \vdots \\
	E_{n-1} x = 0 \\
	E_n x - v = 0 \\
	0 \leqslant x \leqslant u
}
\qquad (\mathrm{con}\ s = 1\ \mathrm e\ t = n)
\end{equation}
dove $v$ è la variabile associata al flusso sull'arco $(t,s)$. Come di consueto si deve eliminare un'equazione di bilancio qualsiasi dal sistema perché esso risulta sovradeterminato.

Usando l'algoritmo del simplesso per flussi per risolvere il problema \eqref{eq:mfmccomemcf} otterremo che l'arco $(t,s)$ sarà sicuramente contenuto in $T$. Esso crea con gli altri archi del grafo un ciclo di costo $-1$ che porta a voler massimizzare il flusso su di esso.

\subsection{Taglio di capacità minima}

Il problema duale del flusso massimo viene chiamato problema del taglio di capacità minima ed è così formulato
\begin{equation} \label{eq:mincut} \tag{mc}
\sistema{
	\displaystyle \min\ u \mu \\
	\trasp E \pi + \mu \geqslant 0 \\
	\pi_t - \pi_s = 1 \\
	\mu \geqslant 0
}
\end{equation}

\begin{definizione}[Taglio] \label{def:taglio}
Chiamiamo taglio di un grafo una bipartizione dei nodi $N = N_s \cup N_t$ tale che $N_s \cap N_t = \emptyset$.
Un taglio si dice ammissibile se $s \in N_s$ e $t \in N_t$.
\end{definizione}

\begin{definizione}[Flusso e capacità di taglio] \label{def:flussocapacitàtaglio}
Dato un taglio $(N_s,N_t)$, definiamo così gli insiemi degli archi diretti ($A^+$) e inversi ($A^-$) del taglio:
$$ A^+ = \{ (i,j) \in A :\ i \in N_s,\ j \in N_t \}
\qquad
A^- = \{ (i,j) \in A :\ i \in N_t,\ j \in N_s \} $$
Allora, la capacità del taglio è
$$ u(N_s, N_t) = \sum_{(i,j) \in A^+} u_{ij} $$
e il flusso che lo attraversa è
$$ x(N_s, N_t) = \sum_{(i,j) \in A^+} x_{ij} - \sum_{(i,j) \in A^-} x_{ij} $$
\end{definizione}

Ad esempio, disegniamo un taglio ammissibile (\raisebox{3pt}{\tikz\draw[thick,dashed] (0,0) -- (2em,0);}) e uno non ammissibile (\raisebox{3pt}{\tikz\draw[thick,dotted] (0,0) -- (2em,0);}) sul grafo di esempio della sezione precedente:
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {7} (2)
			edge node {13} (3);
\path (2)	edge node {6} (3)
			edge node {7} (4)
			edge node {2} (5);
\path (3)	edge node {5} (5)
			edge node {7} (6);
\path (4)	edge node {2} (5)
			edge node {5} (7);
\path (5)	edge node {3} (6)
			edge node {10} (7);
\path (6)	edge node {8} (7);

\draw[dashed] ($(2) +(-1,0)$) -- ($(6) +(0,-1)$);
\draw[dotted] ($(3) +(-2,.7)$) -- ($(6) +(2,.7)$);
\end{tikzpicture} \end{center}
Taglio ammissibile:
$$ N_s = \{1,3\},\ N_t = \{2,4,5,6,7\} \qquad A^+ = \{ (1,2), (3,5), (3,6) \},\ A^- = \{ (2,3) \} $$
$$ u(N_s, N_t) = u_{12} + u_{35} + u_{36} = 19 \qquad x(N_s, N_t) = x_{12} + x_{35} + x_{36} - x_{23} $$

%Taglio non ammissibile:
%$$ N_s = \{3,6\},\ N_t = \{1,2,4,5,7\} \qquad A^+ = \{ (3,5), (6,7) \},\ A^- = \{ (1,3), (2,3), (5,6) \} $$
%$$ u(N_s,N_t) = u_{35} + u_{67} = 13 \qquad x(N_s, N_t) = x_{35} + x_{67} - x_{13} - x_{23} - x_{56} $$

\begin{teorema}[Dualità debole per il flusso massimo] \label{teo:dualitàdebolemfmc}
Dato un grafo capacitato con un nodo sorgente e uno destinazione, se $x$ è un flusso ammissibile e $(N_s, N_t)$ è un taglio ammissibile, allora
$$ v = x(N_s,N_t) \leqslant u(N_s, N_t) $$
\end{teorema}
\begin{dimostrazione}
Dimostriamo la prima parte dell'equazione, cioè $v = x(N_s,N_t)$. Questo è facilmente ottenibile sommando le equazioni di bilancio dei nodi di $N_t$:
$$ E_i x = b_i = \sistema[l @{\quad} l]{
	0 & \mathrm{se\ } i \neq t \\
	v & \mathrm{se\ } i = t
},\ \forall i \in N_t
\qquad \longrightarrow \qquad
\sum_{i \in N_t} E_i x = v $$
ottenendo un'equazione di bilancio del taglio, che sarà uguale alla somma algebrica dei flussi ``entranti'' in $N_t$, cioè
$$ v = \sum_{i \in N_t} E_i x = \sum_{(i,j) \in A^+} x_{ij} - \sum_{(i,j) \in A^-} x_{ij} = x(N_s,N_t) $$

La disuguaglianza $x(N_s,N_t) \leqslant u(N_s, N_t)$ è facilmente verificabile per il vincolo $0 \leqslant x \leqslant u$:
$$ x(N_s,N_t) = \sum_{(i,j) \in A^+} x_{ij} - \sum_{(i,j) \in A^-} x_{ij} \leqslant \sum_{(i,j) \in A^+} x_{ij} \leqslant \sum_{(i,j) \in A^+} u_{ij} = u(N_s,N_t) $$
\end{dimostrazione}

\begin{teorema}[Max flow -- min cut] \label{teo:mfmc}
Dato un grafo capacitato con un nodo sorgente e uno destinazione, se $x$ è un flusso ammissibile e $(N_s, N_t)$ è un taglio ammissibile tali che
$$ x(N_s, N_t) = u(N_s, N_t) $$
allora $x$ è un flusso di valore massimo (\emph{Max flow}) e $(N_s, N_t)$ è un taglio di capacità minima (\emph{min cut}).
\end{teorema}

\subsection{Algoritmo di Ford--Fulkerson}

Introduciamo prima i concetti sui quali si basa l'algoritmo:

\begin{definizione}[Grafo residuo] \label{def:graforesiduo}
Dato un grafo $G = (N,A)$ e un suo flusso ammissibile $x$, il grafo residuo relativo a $x$ è un grafo $G(x) = (N,A(x))$, dove $A(x)$ è formato dagli archi
$$ A(x) = \{ (i,j) \in A : x_{ij} < u_{ij} \} \cup \{ (j,i) : (i,j) \in A,\ x_{ij} > 0 \} $$
Le capacità $r_{ij}$ degli archi di $A(x)$, dette capacità residue, sono così definite:
$$ r_{ij} = \sistema[l @{\quad} l]{
	u_{ij} - x_{ij} & \mathrm{se\ } (i,j) \in A,\ x_{ij} < u_{ij} \\
	x_{ji}			& \mathrm{se\ } (j,i) \in A,\ x_{ji} > 0

} $$
\end{definizione}

\begin{definizione}[Cammino aumentante] \label{def:camminoaumentante}
Dato un flusso $x$, un cammino aumentante è un cammino orientato nel grafo residuo $G(x)$ che va da $s$ a $t$.
\end{definizione}

L'algoritmo quindi procede secondo questa logica: ad ogni iterazione cerca sul grafo residuo un cammino aumentante rispetto al flusso corrente. Se esso esiste, aggiorna il flusso in modo da aumentarlo sugli archi che compongono il cammino, di una quantità pari alla minima capacità degli archi del cammino. Se invece non esiste allora il flusso corrente è il massimo possibile e termina.

\begin{enumerate}
\item Inizializzare $x \leftarrow 0$ e $G(x) \leftarrow G(0) = G$.
\item Trovare un cammino aumentante $CA$ su $G(x)$. Se esso non esiste, \textsc{stop $\Longrightarrow\ x$ è un flusso di valore massimo}. Altrimenti:
\item Calcolare $\delta = \min\ \{r_{ij} : (i,j) \in CA\}$ e aggiornare il flusso in base alla seguente regola:
$$ x_{ij} \leftarrow \sistema[l @{\quad} l]{
	x_{ij} + \delta	& \mathrm{se\ } (i,j) \in CA \\
	x_{ij} - \delta	& \mathrm{se\ } (j,i) \in CA \\
	x_{ij}			& \mathrm{altrimenti}
} $$
\item Aggiornare il grafo residuo $G(x)$ e ritornare al punto 2.
\end{enumerate}

\subsection{Procedura di Edmonds--Karp}

Presentiamo ora un algoritmo di ricerca dei cammini aumentanti su un grafo residuo, utilizzabile al passo 2 dell'algoritmo di Ford--Fulkerson.

\begin{esempio}
Applichiamo l'algoritmo di Ford--Fulkerson al grafo di esempio, usando la procedura di Edmonds--Karp per trovare i cammini aumentanti.

All'inizio, il grafo residuo coincide con il grafo di partenza:
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=2.5cm,thick,bend angle=10,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {7} (2)
			edge node {13} (3);
\path (2)	edge node {6} (3)
			edge node {7} (4)
			edge node {2} (5);
\path (3)	edge node {5} (5)
			edge node {7} (6);
\path (4)	edge node {2} (5)
			edge node {5} (7);
\path (5)	edge node {3} (6)
			edge node {10} (7);
\path (6)	edge node {8} (7);
\end{tikzpicture} \end{center}
Troviamo un cammino aumentante:
$$ \left\{ \begin{array}{cc|cc|c|c}
\circled 1 &   & \circled 2 &   & 3 & \circled 4 \\ \hline
\cancel 2 & \cancel 3 & \cancel 4 & 5 & 6 & \circled 7
\end{array} \right\} \longrightarrow (1-2-4-7)
\qquad \delta = 5 $$
la cui capacità massima è 5. Aggiorniamo il flusso e il grafo residuo:
$$ \begin{array}{r @{\;} *9{c @{,\;}} *2{c @{,\;}} c @{\;} l}
	 A = \{ & (1,2) & (1,3) & (2,3) & (2,4) & (2,5) & (3,5) & (3,6) & (4,5) & (4,7) & (5,6) & (5,7) & (6,7) & \} \\
	  x = ( &   5   &   0   &   0   &   5   &   0   &   0   &   0   &   0   &   5   &   0   &   0   &   0   & )  \\
	r_- = ( &   2   &  13   &   6   &   2   &   2   &   5   &   7   &   2   &   0   &   3   &  10   &   8   & )
\end{array}
\quad
v = 5 $$
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=2.5cm,thick,bend angle=10,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge [bend left] node {2} (2)
			edge node {13} (3);
\path (2)	edge node {6} (3)
			edge [bend left] node {2} (4)
			edge node {2} (5)
			edge [bend left] node {5} (1);
\path (3)	edge node {5} (5)
			edge node {7} (6);
\path (4)	edge node {2} (5)
			edge [bend left] node {5} (2);
\path (5)	edge node {3} (6)
			edge node {10} (7);
\path (6)	edge node {8} (7);
\path (7)	edge node {5} (4);
\end{tikzpicture} \end{center}
%
\hrule % -----------------------------------------------------------------------
%
$$ \left\{ \begin{array}{cc|cc|c|c|c}
\circled 1 &   & \circled 2 &   & 3 & 4 & \circled 5 \\ \hline
\cancel 2 & \cancel 3 & \cancel 4 & \cancel 5 & 6 &   & \circled{7}
\end{array} \right\} \longrightarrow (1-2-5-7)
\qquad \delta = 2 $$
$$ \begin{array}{r @{\;} *9{c @{,\;}} *2{c @{,\;}} c @{\;} l}
	 A = \{ & (1,2) & (1,3) & (2,3) & (2,4) & (2,5) & (3,5) & (3,6) & (4,5) & (4,7) & (5,6) & (5,7) & (6,7) & \} \\
	  x = ( &   7   &   0   &   0   &   5   &   2   &   0   &   0   &   0   &   5   &   0   &   2   &   0   & )  \\
	r_- = ( &   0   &  13   &   6   &   2   &   0   &   5   &   7   &   2   &   0   &   3   &   8   &   8   & )
\end{array}
\quad
v = 7 $$
%
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=2.5cm,thick,bend angle=10,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {13} (3);
\path (2)	edge node {6} (3)
			edge [bend left] node {2} (4)
			edge node {7} (1);
\path (3)	edge node {5} (5)
			edge node {7} (6);
\path (4)	edge node {2} (5)
			edge [bend left] node {5} (2);
\path (5)	edge node {3} (6)
			edge [bend left] node {8} (7)
			edge node {2} (2);
\path (6)	edge node {8} (7);
\path (7)	edge node {5} (4)
			edge [bend left] node {2} (5);
\end{tikzpicture} \end{center}
%
\hrule % -----------------------------------------------------------------------
%
$$ \left\{ \begin{array}{c|cc|cc}
\circled 1 & \circled 3 &   & \circled 5 & \\ \hline
\cancel 3 & \cancel 5 & 6 & 2 & \circled 7
\end{array} \right\} \longrightarrow (1-3-5-7)
\qquad \delta = 5 $$
$$ \begin{array}{r @{\;} *9{c @{,\;}} *2{c @{,\;}} c @{\;} l}
	 A = \{ & (1,2) & (1,3) & (2,3) & (2,4) & (2,5) & (3,5) & (3,6) & (4,5) & (4,7) & (5,6) & (5,7) & (6,7) & \} \\
	  x = ( &   7   &   5   &   0   &   5   &   2   &   5   &   0   &   0   &   5   &   0   &   7   &   0   & )  \\
	r_- = ( &   0   &   8   &   6   &   2   &   0   &   0   &   7   &   2   &   0   &   3   &   3   &   8   & )
\end{array}
\quad
v = 12 $$
%
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=2.5cm,thick,bend angle=10,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge [bend left] node {8} (3);
\path (2)	edge node {6} (3)
			edge [bend left] node {2} (4)
			edge node {7} (1);
\path (3)	edge node {7} (6)
			edge [bend left] node {5} (1);
\path (4)	edge node {2} (5)
			edge [bend left] node {5} (2);
\path (5)	edge node {3} (6)
			edge [bend left] node {3} (7)
			edge node {2} (2)
			edge node {5} (3);
\path (6)	edge node {8} (7);
\path (7)	edge node {5} (4)
			edge [bend left] node {7} (5);
\end{tikzpicture} \end{center}
%
\hrule % -----------------------------------------------------------------------
%
$$ \left\{ \begin{array}{c|c|c}
\circled 1 & \circled 3 & \circled 6 \\ \hline
\cancel 3 & \cancel 6 & \circled 7
\end{array} \right\} \longrightarrow (1-3-6-7)
\qquad \delta = 7 $$
$$ \begin{array}{r @{\;} *9{c @{,\;}} *2{c @{,\;}} c @{\;} l}
	 A = \{ & (1,2) & (1,3) & (2,3) & (2,4) & (2,5) & (3,5) & (3,6) & (4,5) & (4,7) & (5,6) & (5,7) & (6,7) & \} \\
	  x = ( &   7   &  12   &   0   &   5   &   2   &   5   &   7   &   0   &   5   &   0   &   7   &   7   & )  \\
	r_- = ( &   0   &   1   &   6   &   2   &   0   &   0   &   0   &   2   &   0   &   3   &   3   &   1   & )
\end{array}
\quad
v = 19 $$
%
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=2.5cm,thick,bend angle=10,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge [bend left] node {1} (3);
\path (2)	edge node {6} (3)
			edge [bend left] node {2} (4)
			edge node {7} (1);
\path (3)	edge [bend left] node {12} (1);
\path (4)	edge node {2} (5)
			edge [bend left] node {5} (2);
\path (5)	edge node {3} (6)
			edge [bend left] node {3} (7)
			edge node {2} (2)
			edge node {5} (3);
\path (6)	edge [bend left] node {1} (7)
			edge node {7} (3);
\path (7)	edge node {5} (4)
			edge [bend left] node {7} (5)
			edge [bend left] node {7} (6);
\end{tikzpicture} \end{center}
%
\hrule % -----------------------------------------------------------------------
%
$$ \left\{ \begin{array}{c|c}
1 & 3 \\ \hline
\cancel 3 & 
\end{array} \right\}
\longrightarrow
\begin{array}{l} N_s = \{1,3\} \\ N_t = \{2,4,5,6,7\} \end{array} $$

Disegnamo quindi il flusso massimo (gli archi ondulati sono quelli saturi) e il taglio di capacità minima. Notare che tutti gli archi diretti del taglio sono saturi e quelli inversi sono vuoti, cioè $x(N_s,N_t) = u(N_s,N_t)$.
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,on grid,pin distance=5pt,saturo/.style={decorate,decoration={snake,pre length=5pt,post length=10pt}}]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [label=left:{$s=$}] {1};
\node (2) [above right=of 1] {2};
\node (3) [below right=of 1] {3};
\node (5) [below right=of 2] {5};
\node (4) [above right=of 5] {4};
\node (6) [below right=of 5] {6};
\node (7) [below right=of 4,label=right:{$=t$}] {7};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge [saturo] node {7} (2)
			edge node {12} (3);
\path (2)	edge node {0} (3)
			edge node {5} (4)
			edge [saturo] node {2} (5);
\path (3)	edge [saturo] node {5} (5)
			edge [saturo] node {7} (6);
\path (4)	edge node {0} (5)
			edge [saturo] node {5} (7);
\path (5)	edge node {0} (6)
			edge node {7} (7);
\path (6)	edge node {7} (7);

\draw[dashed] ($(2) +(-1,0)$) -- ($(6) +(0,-1)$);
\end{tikzpicture} \end{center}
\begin{comment}
A	12	13	23	24	25	35	36	45	47	56	57	67
===============================================================
x	0	0	0	0	0	0	0	0	0	0	0	0
r	7	13	6	7	2	5	7	2	5	3	10	8	START
	----------------------------------------------
x	5	0	0	5	0	0	0	0	5	0	0	0
r	2	13	6	2	2	5	7	2	0	3	10	8	1-2-4-7 (5)
	----------------------------------------------
x	7	0	0	5	2	0	0	0	5	0	2	0
r	0	13	6	2	0	5	7	2	0	3	8	8	1-2-5-7 (2)
	----------------------------------------------
x	7	5	0	5	2	5	0	0	5	0	7	0
r	0	8	6	2	0	0	7	2	0	3	3	8	1-3-5-7 (5)
	----------------------------------------------
x	7	12	0	5	2	5	7	0	5	0	7	7
r	0	1	6	2	0	0	0	2	0	3	3	1	1-3-6-7 (7)
===============================================================
	Ns = {1,3} Nt = {2,4,5,6,7}						TOT = 19


1	2+
	3+
2	4+
	5
3	6
4	7

1-2-4-7 (5)

1	2+
	3+
2	4+
	5+
3	6
4
5	7

1-2-5-7 (2)

1	3+
3	5+
	6
5	7

1-3-5-7 (5)

1	3+
3	6+
6	7

1-3-6-7 (7)

1	3+
3

STOP
\end{comment}
\end{esempio}