\section{Flusso di costo minimo su reti capacitate}

Consideriamo un grafo $G=(N,A)$ costituito da $n$ nodi ed $m$ archi (orientati). Gli archi sono coppie ordinate di nodi. Ad ogni arco $(i,j)$ del grafo associamo un costo $c_{ij}$ ed una capacità massima $u_{ij}$. Ad ogni nodo $i$ associamo un bilancio $b_i$, convenzionalmente positivo se il nodo è un pozzo (sink) e negativo se è una sorgente (source). Supponiamo sempre che un grafo sia bilanciato, cioè $\sum b_i = 0$.

\begin{esempio}
%\renewcommand{\arraystretch}{1.4}
$$ \begin{array}{r@{\;}*5{r@{,\;}}r@{\;}l}
N = \{ &  1 &  2 &  3 &  4 &  5 &  6 & \} \\
b = (  & -4 & -2 & -1 &  \phantom - 3 &  \phantom - 2 &  \phantom - 2 & ) 
\end{array} $$
$$\begin{array}{r@{\;}*8{c@{,\;}}c@{\;}l}
A = \{ & (1,2) & (1,3) & (2,4) & (2,5) & (3,4) & (3,6) & (4,5) & (4,6) & (6,5) & \} \\
c = (  &   6   &   5   &   3   &   3   &   6   &   1   &   4   &   6   &   2   & )  \\
u = (  &   9   &   7   &   8   &   9   &   9   &  12   &   2   &  11   &  10   & )
\end{array} $$
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3.5cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [pin=left:-4] {1};
\node (2) [pin=above:-2,above right=of 1] {2};
\node (3) [pin=below:-1,below right=of 1] {3};
\node (4) [pin=above:3,below right=of 2] {4};
\node (5) [pin=above right:2,above right=of 4] {5};
\node (6) [pin=below right:2,below right=of 4] {6};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1) 	edge node {$(6,9)$} (2)
			edge [swap] node {$(5,7)$} (3);
\path (2)	edge node {$(3,9)$} (5)
			edge [swap] node {$(3,8)$} (4);
\path (3)	edge node {$(6,9)$} (4)
			edge [swap] node {$(1,12)$} (6);
\path (4)	edge [swap] node {$(4,2)$} (5)
			edge node {$(6,11)$} (6);
\path (6)	edge [swap,bend right] node {$(2,10)$} (5);
\end{tikzpicture} \end{center}
\end{esempio}

Consideriamo il problema di calcolare un flusso di costo minimo che rispetti i vincoli di bilancio ai nodi e la capacità degli archi (assieme ovviamente al loro verso). Il costo di un flusso è dato dalla somma dei costi dei singoli archi moltiplicati per le unità di flusso sull'arco. Il problema può essere formulato così:

\begin{equation} \label{eq:mcf} \tag{MCF}
\sistema{
	\min\ cx \\
	Ex = b \\
	0 \leqslant x \leqslant u
}
\end{equation}

Il sistema \eqref{eq:mcf} può essere facilmente portato in forma duale standard trasformando le disuguaglianze $x \leqslant u$ in uguaglianze con variabili di scarto vincolate in segno $x + w = u,\ w \geqslant 0$:
\begin{equation} \label{eq:mcfdualestd}
\sistema{
	\min\ cx \\
	Ex = b \\
	x + w = u \\
	x, w \geqslant 0
}
\end{equation}

La variabile $x \in R^m$ è il vettore del flusso e $E \in \R^{n \times m}$ è la matrice di incidenza. Le $n$ equazioni $Ex = b$ sono chiamate equazioni di bilancio. Di seguito l'equazione di bilancio per il nodo 4 del grafo di esempio:
$$ x_{24} + x_{34} - x_{45} - x_{46} = b_4 = 3 $$
\`E la somma algebrica dei flussi entranti nel nodo 4, in cui le variabili di flusso corrispondenti ad archi entranti hanno coefficiente 1 e -1 quelle corrispondenti ad archi uscenti (0 le altre).
La matrice di incidenza del grafo di esempio sarà
$$ E = \left[ \begin{array}{*9c}
-1 & -1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 
1 & 0 & -1 & -1 & 0 & 0 & 0 & 0 & 0 \\ 
0 & 1 & 0 & 0 & -1 & -1 & 0 & 0 & 0 \\ 
0 & 0 & 1 & 0 & 1 & 0 & -1 & -1 & 0 \\ 
0 & 0 & 0 & 1 & 0 & 0 & 1 & 0 & 1 \\ 
0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & -1
\end{array} \right]
%\ \begin{array}{c} 1 \\ 2 \\ 3 \\ 4 \\ 5 \\ 6 \end{array}
$$

Notare che, essendo ogni colonna relativa a un arco, avrà tutti elementi 0, un $-1$ (da) e un 1 (a). Alla luce di questo, si vede chiaramente che la somma di tutte le righe dà sempre zero, quindi sicuramente $\rank E < n$. Questo significa che il sistema è sovradeterminato e che è possibile eliminare un'equazione. Di seguito supporremo sempre che ad $E$ sia stata tolta la prima riga.

\begin{definizione}[Grafo connesso]
Un grafo si dice connesso se presi due nodi $h,k$ qualsiasi del grafo, esiste sempre un cammino non orientato che va da $h$ a $k$.
\end{definizione}

\begin{definizione}[Albero di copertura]
Un albero di copertura per un grafo $G = (N,A)$ è un sottoinsieme $T \subseteq A$ dei suoi archi tale che $(N,T)$ è un grafo orientato e privo di cicli. Inoltre, un alberto di copertura contiene esattamente $n - 1$ archi.
\end{definizione}

Il grafo di esempio all'inizio del capitolo è orientato e un suo possibile albero di copertura è $T = \{(1,3), (2,4), (3,4), (4,5), (6,5)\}$:

\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3.5cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [pin=left:-4] {1};
\node (2) [pin=above:-2,above right=of 1] {2};
\node (3) [pin=below:-1,below right=of 1] {3};
\node (4) [pin=above:3,below right=of 2] {4};
\node (5) [pin=above right:2,above right=of 4] {5};
\node (6) [pin=below right:2,below right=of 4] {6};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1) edge [swap] node {$(5,7)$} (3);
\path (2) edge [swap] node {$(3,8)$} (4);
\path (3) edge node {$(6,9)$} (4);
\path (4) edge [swap] node {$(4,2)$} (5);
\path (6) edge [swap,bend right] node {$(2,10)$} (5);
\end{tikzpicture} \end{center}

\begin{teorema}[Rango della matrice di incidenza] \label{teo:rangoE}
Dato un albero di copertura $T$ per un grafo $G=(N,A)$, la matrice $E_T$, estratta dalla matrice di incidenza di $G$ (privata della prima riga) selezionando le sole colonne relative agli archi di $T$, è quadrata di dimensione $n - 1$ ed ha determinante $\pm 1$. Quindi, $\rank\ E_T = n - 1$.
\end{teorema}

Il teorema si può dimostrare per induzione sulla matrice di incidenza: se si permuta opportunamente $E_T$ portando in alto le righe corrispondenti alle foglie di $T$ e a sinistra le colonne corrispondenti agli archi incidenti su di esse, si ottiene una matrice triangolare inferiore nella quale gli elementi sulla diagonale sono tutti $\pm 1$. Il fatto che la matrice di incidenza sia permutabile fino ad ottenere una triangolare ci dice che il solo sistema $E_T x = b$ è facile da risolvere perché può essere facilmente portato in forma triangolare. Questa linea di dimostrazione ci fornisce anche un metodo per risolvere $E_T x = b$ tramite ispezione visiva: effettuando quella che si chiama una ``visita per foglie'', risolviamo man mano le equazioni di bilancio ai nodi, sostituendo i risultati intermedi nelle successive.

Vale anche l'inverso del \thref{teo:rangoE}, cioè:

\begin{teorema} \label{teo:rangoE2}
Se $E_T$ è una sottomatrice di $E$, invertibile e di dimensione $n - 1$, allora $T$ è un albero di copertura.
\end{teorema}

\begin{esempio}
Calcoliamo il flusso sull'albero di copertura di esempio al paragrafo precedente. Il sistema da risolvere è questo, nella sua forma originale e riordinato per essere triangolare:
$$ \sistema[l@{\quad}l]{
(1) & \cancel{- x_{13} = -4} \\
(2) & - x_{24} = -2 \\
(3) & x_{13} - x_{34} = -1 \\
(4) & x_{24} + x_{34} - x_{45} = 3 \\
(5) & x_{45} + x_{65} = 2 \\
(6) & - x_{65} = 2
}
\quad
\sistema[l@{\quad}l]{
(2) & - x_{24} = -2 \\
(6) & - x_{65} = 2 \\
(5) & x_{45} + x_{65} = 2 \\
(4) & x_{24} + x_{34} - x_{45} = 3 \\
(3) & x_{13} - x_{34} = -1
}
\quad
\sistema{
	x_{24} = 2 \\
	x_{65} = -2 \\
	x_{45} = 4 \\
	x_{34} = 5 \\
	x_{13} = 4
}
$$
La soluzione ottenuta non è ammissibile perché $x_{65} < 0$ e $x_{45} > u_{45} = 2$.
\end{esempio}

\subsection{Flussi di base}

Riscriviamo il sistema \eqref{eq:mcfdualestd} in forma matriciale\footnote{Ricordare che la matrice $E$ contiene $n - 1$ righe!}:
$$ \sistema{
\min\ cx \\
\left[ \begin{array}{cc} E & 0_{n-1}^m \\ I_m & I_m \end{array} \right]
\left[ \begin{array}{c} x \\ w \end{array} \right]
=
\left[ \begin{array}{c} b \\ u \end{array} \right] \\
x, w \geqslant 0
}
\qquad
\trasp M = \left[ \begin{array}{cc} E & 0_{n-1}^m \\ I_m & I_m \end{array} \right]
$$
Nella scrittura a blocchi di $\trasp M$ sono specificate le dimensioni dei singoli blocchi: $I_m \in \R^{m \times m},\ 0_{n-1}^m \in \R^{(n-1) \times m}$. In totale, $M$ ha dimensione $(m+m) \times (n-1+m)$. Inoltre, per il \thref{teo:rangoE} e per come è costruita, $M$ ha rango $n + m - 1$.

\begin{teorema}[Caratterizzazione delle basi per reti capacitate]
Presa una tripartizione $(T,L,U)$ degli archi, con $T$ albero di copertura, e indicando con $T',L',U'$ gli indici relativi a detta tripartizione per le variabili $w$, suddividiamo le colonne di $\trasp M$ in questo modo:
\begin{equation} \label{eq:tutl}
\trasp M = 
\matrice{c|c}{ E & 0 \\ \hline I & I } =
\left[ \begin{array}{ccc|ccc}
	E_T & E_L & E_U &   0    &   0    &  0     \\
	                  \hline
	I_T &  0  &  0  & I_{T'} &   0    &  0     \\
	 0  & I_L &  0  &   0    & I_{L'} &  0     \\
	 0  &  0  & I_U &   0    &   0    & I_{U'}
\end{array} \right]
\end{equation}
$$ (x,w) = (x_T, x_L, x_U, w_T, w_L, w_U) $$
Si consideri una base così formata e la sua corrispondente matrice:
\begin{equation} \label{eq:btutl}
	B = T \cup U \cup T' \cup L'
	\qquad
	\trasp M_B = \left[ \begin{array}{cc|cc}
		E_T	& E_U	& 0 	& 0 		\\ 
					\hline
		I_T	& 0		& I_{T'} & 0 		\\ 
		0	& 0		& 0 	& I_{L'} 	\\ 
		0	& I_U	& 0 	& 0
	\end{array} \right]
	\quad
	\left[ \begin{array}{c} \bar x \\ \bar w \end{array} \right]_B =
	\left[ \begin{array}{c} \bar x_T \\ \bar x_U \\ \bar w_T \\ \bar w_L \end{array} \right]
\end{equation}
Allora $\det\ M_B = \pm 1$ e quindi costituisce una base valida per il problema.
\end{teorema}

Scriviamo le equazioni relative a una base $(T,L,U)$:
\begin{equation} \label{eq:flussobase}
\sistema{
	E_T \bar x_T + E_U \bar x_U = b \\
	\bar x_T + \bar w_T = u_T \\
	\bar w_L = u_L \\
	\bar x_U = u_U \\
	\bar x_L, \bar w_U = 0
}
\quad \Longrightarrow \quad
\sistema{
	\bar x_T = E_T^{-1} (b - E_U u_U) \\
	\bar x_L = 0 \\
	\bar x_U = u_U
}
\end{equation}
Tornando alla \eqref{eq:mcf} ricordiamo che una soluzione (flusso) è ammissibile se $0 \leqslant \bar x \leqslant u$. Questo è banalmente vero per $\bar x_L$ e $\bar x_U$. La sola disequazione 
\begin{equation} \label{eq:flussoammissibile}
0 \leqslant \bar x_T \leqslant u_T
\end{equation}
costituisce quindi la condizione di ammissibilità per il flusso di base relativo a una tripartizione $(T,L,U)$.

Un flusso di base si dice invece degenere se almeno una delle variabili in base è zero. Tutte le componenti di $\bar x_U = u_U$ e $\bar w_L = u_L$ sono positive perché supponiamo ragionevolmente che le capacità superiori siano sempre strettamente positive. Se invece una componente $(i,j)$ di $\bar x_T$ o di $\bar w_T$ è zero, allora significa che
$$ (\bar x_{ij} = 0) \vee (\bar x_{ij} = u_{ij}),\quad (i,j) \in T $$
cioè un arco in $T$ è \emph{vuoto o saturo}.

\begin{teorema}[Interezza] \label{teo:interezza}
In un problema di flusso di costo minimo, se i vettori $u$ e $b$ sono a componenti intere, allora anche i flussi di base saranno a componenti intere.
\end{teorema}
\begin{dimostrazione}
Presa la prima equazione della \eqref{eq:flussobase} e ricordando che $E$ è formata da tutti $0$ e $\pm 1$, allora il vettore $b - E_U u_U$ è sicuramente a componenti intere, così come $E_T^{-1}$, poiché abbiamo già visto che $\det E_T = \pm 1$. Segue la tesi.
\end{dimostrazione}

\subsection{Potenziali di base}

Calcoliamo il problema duale del \eqref{eq:mcfdualestd}:
\begin{equation} \label{eq:potenziale}
\sistema{
	\max\ b \pi + u \mu \\
	\trasp E \pi + \mu \leqslant c \\
	\mu \leqslant 0
}
\qquad
\sistema{
	\max\ b\pi + u\mu \\
	\matrice{cc}{\trasp E & I \\ 0 & I} \vettore{\pi \\ \mu} \leqslant \vettore{c \\ 0}
}
\end{equation}
Notare le dimensioni delle variabili: $\pi \in \R^n,\ \mu \in \R^{m}$ e ricordare che $\rank\ E = n-1$.

Calcoliamo la relativa soluzione di base per $B = T \cup U \cup T' \cup L'$:
\begin{equation} \label{eq:potenzialebase}
\sistema{
	\trasp E_T \bar\pi + \bar\mu_T = c_T \\
	\trasp E_U \bar\pi + \bar\mu_U = c_U \\
	\bar\mu_T,\ \bar\mu_L = 0
}
\quad \Longrightarrow \quad
\sistema{
	\bar\pi = \big(\trasp E_T\big)^{-1} c_T \\
	\bar\mu_T = 0 \\
	\bar\mu_L = 0 \\
	\bar\mu_U = c_U - \trasp E_U \bar\pi
}
\end{equation}
Il primo sistema $\trasp E_T \bar\pi + \bar\mu_T = c_T$ è \emph{sottodeterminato}. Vedremo che questo non è un problema e per il momento fissiamo una componente: $\bar\pi_1 = 0$. La soluzione $(\bar\pi, \bar\mu)$ è ammissibile se i vincoli non in base ($N = L \cup U'$) sono rispettati, ovvero:
\begin{equation} \label{eq:potenzialebaseesplicito}
\sistema{
	\trasp E_L \bar\pi \ \cancel{+ \bar\mu_L} \leqslant c_L \\
	\bar\mu_U = c_U - \trasp E_U \bar\pi \leqslant 0
}
\quad \Longrightarrow \quad
\sistema{
	c_L \geqslant \trasp E_L \bar\pi \\
	c_U \leqslant \trasp E_U \bar\pi
}
\end{equation}
Poiché ogni riga di $\trasp E_L$ è relativa ad un arco $(i,j) \in L$, contiene tutti zeri tranne un 1 in posizione $j$ e un $-1$ in posizione $i$ e lo stesso si può dire di $\trasp E_U$. Possiamo allora esplodere le disequazioni \eqref{eq:potenzialebaseesplicito} in disequazioni scalari:
\begin{equation}
\sistema[l@{\quad}l]{
	c_{ij} \geqslant \bar\pi_j - \bar\pi_i & \forall (i,j) \in L \\
	c_{ij} \leqslant \bar\pi_j - \bar\pi_i & \forall (i,j) \in U
}
\end{equation}
Abbiamo così ottenuto la condizione di ammissibilità per un potenziale di base. Spesso questa stessa condizione è espressa in modo diverso, definendo i cosiddetti ``costi ridotti'':
\begin{equation} \label{eq:bellman}
\crid := c_{ij} - \bar\pi_j + \bar\pi_i
\qquad
\sistema[l@{\quad}l]{
	\crid \geqslant 0 & \forall (i,j) \in L \\
	\crid \leqslant 0 & \forall (i,j) \in U
}
\end{equation}
Le \eqref{eq:bellman} sono chiamate ``condizioni di Bellman'', omonime dei prossimi tre teoremi:

\begin{teorema}[Bellman] \label{teo:bellman}
Data una tripartizione $(T,L,U)$ che genera un flusso di base ammissibile, se, calcolato il potenziale di base, esso soddisfa le condizioni di Bellman \eqref{eq:bellman}, allora il flusso è ottimo.
\end{teorema}

Un potenziale si dice degenere se esiste almeno un arco in $L$ o in $U$ che abbia costo ridotto nullo.

\begin{teorema}[Bellman bis] \label{teo:bellmanbis}
Data una tripartizione $(T,L,U)$ che genera un potenziale di base ammissibile, se, calcolato il flusso di base, esso è ammissibile, ovvero vale la \eqref{eq:flussoammissibile}, allora il potenziale è ottimo.
\end{teorema}

\begin{teorema}[Bellman ter] \label{teo:bellmanter}
Data una tripartizione $(T,L,U)$ che genera un flusso di base ammissibile e \textscsl{non} degenere, allora il flusso è anche ottimo se e solo se il potenziale di base rispetta le condizioni di Bellman \eqref{eq:bellman}.
\end{teorema}

\begin{esempio}
Presentiamo un grafo di esempio e calcoliamo flusso e potenziale di base.

\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm and 4cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [pin=above left:-5] {1};
\node (2) [pin=below left:2,below=of 1] {2};
\node (3) [pin=above:4,right=of 1] {3};
\node (4) [pin=below:-8,right=of 2] {4};
\node (5) [pin=above right:6,right=of 3] {5};
\node (6) [pin=below right:1,right=of 4] {6};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (1)	edge node {$(9,7)$} (3);
\path (2)	edge node {$(6,9)$} (1)
			edge node {$(7,8)$} (3);
\path (3)	edge node {$(8,4)$} (5)
			edge node {$(9,9)$} (6);
\path (4)	edge node {$(5,5)$} (2)
			edge node {$(4,9)$} (3)
			edge [swap] node {$(9,8)$} (6);
\path (6)	edge [swap] node {$(7,2)$} (5);
\end{tikzpicture} \end{center}

Prendiamo ad esempio la base
$$ B = \big(
T = \{ (2,1), (3,6), (4,2), (4,3), (6,5) \},\ 
U = \{ (3,5) \},\ 
L = \{ \ldots \}
\big) $$

\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm and 4cm,thick,bend angle=20,on grid,pin distance=5pt,
u/.style={decorate,decoration={snake,pre length=5pt,post length=10pt}}]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\node (1) [pin=above left:-5] {1};
\node (2) [pin=below left:2,below=of 1] {2};
\node (3) [pin=above:4,right=of 1] {3};
\node (4) [pin=below:-8,right=of 2] {4};
\node (5) [pin=above right:6,right=of 3] {5};
\node (6) [pin=below right:1,right=of 4] {6};

\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\path (2)	edge node {$(6,9)$} (1);
\path (3)	edge [u] node {$(8,4)$} (5)
			edge node {$(9,9)$} (6);
\path (4)	edge node {$(5,5)$} (2)
			edge node {$(4,9)$} (3);
\path (6)	edge [swap] node {$(7,2)$} (5);
\end{tikzpicture} \end{center}

Convenzionalmente indichiamo con un arco ondulato (\tikz{ \draw[-latex',thick,decorate,decoration={snake,post length=3pt,pre length=2pt}] (0,0) -- (3em,0); }) un arco appartenente ad $U$ (cioè saturo). Calcoliamo $\bar x$ e $\bar\pi$:
$$\begin{array}{r @{\;} *8{c @{,\;}} c @{\;} l}
    A = \{ & (1,3) & (2,1) & (2,3) & (3,5) & (3,6) & (4,2) & (4,3) & (4,6) & (6,5) & \} \\
\bar x = ( &   0   &  -5   &   0   &   4   &   3   &  -3   &  11   &   0   &   2   & )
\end{array} \quad \mathrm{N/AMM,\ DEG}$$
$$ \begin{array}{r @{\;} *5{r @{,\;}} r @{\;} l}
     N = \{ & 1 &  2 &  3 &  4 &  5 &  6 & \} \\
\bar\pi = ( & 0 & -6 & -7 & -11 & \phantom - 9 & \phantom - 2 & ) 
\end{array} \quad \mathrm{N/AMM,\ DEG}$$
\end{esempio}

Presentiamo anche il teorema degli scarti complementari per il problema di flusso di costo minimo. Si osservi che il risultato è molto simile al teorema \ref{teo:bellmanter}, ma differente perché fa riferimento a soluzioni in scarti complementari, non necessariamente generate dalla stessa base.
\begin{teorema}[Scarti complementari per flussi e potenziali]
Un flusso ammissibile $x$ è ottimo se e solo se esiste un potenziale $\pi$ tale che
$$ \sistema[l @{\quad} l]{
	c^\pi_{ij} \geqslant 0 	& \mathrm{se\ } x_{ij} = 0 			\\
	c^\pi_{ij} = 0 			& \mathrm{se\ } 0 < x_{ij} < i_{ij}	\\
	c^\pi_{ij} \leqslant 0	& \mathrm{se\ } x_{ij} = u_{ij}
} $$
\end{teorema}

\subsection{Algoritmo del simplesso per flussi su reti capacitate}

L'algoritmo del simplesso (duale) per reti capacitate parte da una tripartizione $(T,L,U)$ che genera un flusso di base ammissibile, dopodiché calcola il potenziale di base e, se questo è ammissibile, allora termina perché ha raggiunto una base ottima. Altrimenti, seleziona l'arco entrante in $T$ e l'arco uscente (che andrà in $L$ o in $U$), effettua il cambio di base e ripete il procedimento.

Nella descrizione passo passo è stata integrata la regola di aggiornamento immediato del flusso e del potenziale di base, che quindi non vanno ricalcolati all'iterazione successiva, perché rende più chiaro il funzionamento.

\begin{enumerate}
\item Calcolare il flusso e il potenziale di base $\bar x$ e $\bar\pi$ utilizzando le \eqref{eq:flussobase} e \eqref{eq:potenzialebase}.
\item Se $\bar\pi$ verifica le condizioni di Bellman \eqref{eq:bellman}, allora \textsc{stop $\Longrightarrow\ \bar x$ è ottimo}. Altrimenti:
\item Selezionare come arco entrante $(p,q)$ il primo, in ordine lessicografico, tra quelli in $L$ o in $U$ che violano le condizioni di Bellman.
\item Considerare il ciclo orientato $\C$ che $(p,q)$ forma con gli archi presenti in $T$, di verso concorde a $(p,q)$ se $\in L$, discorde se $\in U$. Partizionare $\C = \C^+ \cup \C^-$, dove $\C^+$ è formato dagli archi di $\C$ di verso concorde ad esso e $\C^-$ da quelli di verso discorde.
%\item Se $\C^- = \emptyset$ allora STOP => il flusso ha costo $-\infty$.
\item Calcolare i seguenti valori:
\begin{equation} \label{eq:theta}
\vartheta^+ = \min_{(i,j) \in \C^+} \{u_{ij} - \bar x_{ij}\}\quad
\vartheta^- = \min_{(i,j) \in \C^-} \{\bar x_{ij}\}\quad
\vartheta = \min\ \{\vartheta^+, \vartheta^-\}
\end{equation}
\item Selezionare come arco uscente $(r,s)$ il primo, in ordine lessicografico, tra quelli in $\C^+$ tali che $u_{ij} - \bar x_{ij} = \vartheta$ e quelli in $\C^-$ tali che $\bar x_{ij} = \vartheta$.
\item Aggiornare il flusso e il potenziale di base con la seguente regola:
\begin{equation} \label{eq:aggiornaflussopotenziale}
\bar x_{ij} \leftarrow \sistema[l @{\quad} l]{
	x_{ij} + \vartheta	& \mathrm{se\ } (i,j) \in \C^+ \\
	x_{ij} - \vartheta	& \mathrm{se\ } (i,j) \in \C^- \\
	x_{ij}				& \mathrm{altrimenti}
}
\qquad
\bar\pi_i \leftarrow \sistema[l @{\quad} l]{
	\bar\pi_i				& \mathrm{se\ } i \in N_p \\
	\bar\pi_i + \crid[pq]	& \mathrm{se\ } i \in N_q
}
\end{equation}
dove $N_p$ e $N_q$ sono le due componenti connesse di $T$ che si vengono a creare dopo aver tolto l'arco $(r,s)$ da $T$ stesso, con $p \in N_p$ e $q \in N_q$.
\item Effettuare il cambio di base: $T \leftarrow T \cup (p,q) \setminus (r,s)$. Se $(p,q) \in L$, allora $L \leftarrow L \setminus (p,q)$, altrimenti $U \leftarrow U \setminus (p,q)$.

Inoltre, se $(r,s) \in \C^-$, allora $L \leftarrow L \cup (r,s)$, altrimenti $U \leftarrow U \cup (r,s)$ e ritornare al punto 2.
\end{enumerate}

\begin{teorema}[Correttezza del simplesso per flussi su reti capacitate]
L'algoritmo del simplesso per flussi su reti capacitate risolve il problema \eqref{eq:mcf} in un numero finito di passi.
\end{teorema}
\begin{dimostrazione}
Innanzitutto, evidenziamo l'importanza del \thref{teo:interezza} nel calcolo delle soluzioni di base, che ci permette di omettere i vincoli di interezza quando i vettori $u$ e $b$ sono a componenti intere.

Ad una qualsiasi iterazione, se il flusso e il potenziale sono entrambi ammissibili l'algoritmo termina perché ha raggiunto la soluzione ottima, per il \thref{teo:bellman} di Bellman. Se invece il potenziale non è ammissibile, viene trovato un arco $(p,q)$ che viola le condizioni di Bellman. Esso genera sicuramente un ciclo (chiamiamolo $\C$) con gli archi in $T$ (che, essendo un albero di copertura, è l'insieme minimale di archi necessario a connettere tutti i nodi). Se $(p,q) \in L$, allora il suo flusso può essere aumentato e quindi orientiamo il ciclo concorde al suo verso e il viceversa vale se $(p,q) \in U$.

\begin{center}
\begin{minipage}{0.49\textwidth} \centering \begin{tikzpicture}
	[>=latex',auto,node distance=3cm and 4cm,thick,bend angle=20,on grid,pin distance=5pt]
	
	\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
	\tikzstyle{every pin}=[inner sep=0pt,draw=none]
	\tikzstyle{every pin edge}=[semithick]
	
	% nodi
	\node (p) [pin=above left:$b_p$] {$p$};
	\node (q) [pin=above right:$b_q$,right=of p] {$q$};
	\node (r) [pin=below left:$b_r$,below=of p] {$r$};
	\node (s) [pin=below right:$b_s$,below=of q] {$s$};
	
	\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
	\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
	
	\path (p) edge [dashed] node[below] {$\in L$} node[above] {$+$} (q);
	\path (p) edge node [left] {$-$} (r);
	\path (r) edge node [below] {$-$} (s);
	\path (q) edge node [right] {$+$} (s);
	
	\draw[<-] (2,-.8) arc[radius=.8cm,start angle=90,end angle=440];
\end{tikzpicture} \end{minipage}
\hfill
\begin{minipage}{0.49\textwidth} \centering \begin{tikzpicture}
	[>=latex',auto,node distance=3cm and 4cm,thick,bend angle=20,on grid,pin distance=5pt]
	
	\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
	\tikzstyle{every pin}=[inner sep=0pt,draw=none]
	\tikzstyle{every pin edge}=[semithick]
	
	% nodi
	\node (p) [pin=above left:$b_p$] {$p$};
	\node (q) [pin=above right:$b_q$,right=of p] {$q$};
	\node (r) [pin=below left:$b_r$,below=of p] {$r$};
	\node (s) [pin=below right:$b_s$,below=of q] {$s$};
	
	\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
	\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
	
	\path (p) edge [dashed] node[below] {$\in U$} node[above] {$-$} (q);
	\path (p) edge node [left] {$+$} (r);
	\path (r) edge node [below] {$+$} (s);
	\path (q) edge node [right] {$-$} (s);
	
	\draw[->] (2,-.8) arc[radius=.8cm,start angle=90,end angle=440];
\end{tikzpicture} \end{minipage}
\end{center}

Calcoliamo la massima quantità di flusso che si può sommare algebricamente agli archi del ciclo, nel verso del ciclo stesso, in modo che i vincoli siano rispettati. Vediamo che, per qualsiasi quantità, i vincoli di bilancio ai nodi sono sempre rispettati. Infatti, se un nodo non si trova sul ciclo $\C$, il flusso sugli archi che vi incidono non cambia. Se, invece, si trova sul ciclo, su di esso incidono esattamente due archi del ciclo e quindi distinguiamo i quattro casi base, ricordando la regola di aggiornamento \eqref{eq:aggiornaflussopotenziale}:
\begin{itemize} \setlength{\parskip}{0pt}
	\item \tikzinlinenode{->}{->} Entrambi gli archi sono in $\C^+$ o in $\C^-$ quindi ad una variazione del flusso entrante corrisponde una stessa variazione del flusso uscente.
	\item \tikzinlinenode{<-}{<-} Analogo al precedente.
	\item \tikzinlinenode{->}{<-} Uno degli archi è in $\C^+$ e l'altro è in $\C^-$, quindi la variazione del flusso su un arco è opposta a quella sull'altro e si annullano a vicenda.
	\item \tikzinlinenode{<-}{->} Analogo al precedente.
\end{itemize}
I vincoli di capacità (inferiore e superiore) sono sicuramente rispettati sugli archi non appartenenti a $\C$ perché il loro flusso rimane inalterato, mentre una variazione troppo grande del flusso sul ciclo potrebbe violarli ed infatti entra in gioco l'algoritmo di scelta di $\vartheta$ \eqref{eq:theta}. Sugli archi in $\C^+$ possiamo aumentare la quantità di flusso fino a saturarne uno, mentre sugli archi in $\C^-$ la possiamo diminuire fino a svuotarne uno. Si è quindi dimostrato che la regola di aggiornamento del flusso è corretta, cioè genera un nuovo flusso di base ammissibile.

Per dimostrare la correttezza della regola di aggiornamento del potenziale, consideriamo cosa succede quando l'arco $(r,s)$ viene rimosso da $T$. Se esso è anche l'arco entrante $(p,q)$, allora $\bar\pi$ resta invariato in quanto anche $T$ rimane invariato. Altrimenti, $T = N_p \cup N_q$ viene bipartizionato in modo tale che $p \in N_p,\ q \in N_q$. Chiamiamo $\pi'$ il nuovo potenziale. Poiché esso è definito a meno di una costante additiva, possiamo supporre che rimanga invariato sui nodi di $N_p$. Allora, abbiamo che $\pi'_q = \bar\pi_p + c_{pq} = \bar\pi_q + \crid[pq]$, perché ora $(p,q)$ è in base (nel secondo passaggio basta sostituire la \eqref{eq:bellman}) e lo stesso vale per tutti i nodi in $N_q$, perché sono connessi a $q$ (vedi \eqref{eq:aggiornaflussopotenziale}).

Volendo esaminare la variazione dei costi ridotti, si vede semplicemente che essi rimangono invariati per tutti gli archi che connettono nodi all'interno della stessa partizione, perché abbiamo visto che il potenziale rimane invariato o cambia solo per una costante additiva (che nei costi ridotti si annulla). Sugli archi $(i,j)$ con $i \in N_p,\ j \in N_q$ abbiamo che $\pi'_i = \bar\pi_i$ e $\pi'_j = \bar\pi_j + \crid[pq]$, quindi $c^{\pi'}_{ij} = c_{ij} - \pi'_j + \pi'_i = c_{ij} - \bar\pi_j - \crid[pq] + \bar\pi_i = \crid[ij] - \crid[pq]$. L'analogo vale se $i \in N_q,\ j \in N_p$:
$$
\crid[ij] \leftarrow c^{\pi'}_{ij} = \sistema[l @{\quad} l]{
	\crid[ij]				& \mathrm{se\ } (i,j \in N_p) \vee (i,j \in N_q) 	\\
	\crid[ij] - \crid[pq]	& \mathrm{se\ } i \in N_p,\ j \in N_q 				\\
	\crid[ij] + \crid[pq]	& \mathrm{se\ } i \in N_q,\ j \in N_p
}
$$

Si consideri adesso il valore della funzione obiettivo, ovvero il costo del flusso totale, al variare di $\vartheta$:
$$
c x(\vartheta)
= c \bar x + \vartheta \left[ \sum_{(i,j) \in \C^+} c_{ij} - \sum_{(i,j) \in \C^-} c_{ij} \right]
= \sistema[l @{\quad} l]{
	c \bar x + \vartheta \crid[pq]	& \mathrm{se\ } (p,q) \in L \\
	c \bar x - \vartheta \crid[pq]	& \mathrm{se\ } (p,q) \in U
}
$$
La parte tra parentesi, a meno del costo sull'arco entrante ($c_{pq}$), è la somma algebrica dei costi sugli archi del ciclo, che corrisponde alla differenza di potenziale tra $p$ e $q$, in segno positivo o negativo, cioè il costo ridotto di $(p,q)$ se $\in L$, o il suo opposto se $\in U$. Ricordando come abbiamo scelto $(p,q)$ (non soddisfaceva le condizioni di Bellman), si ha sempre che $cx(\vartheta) < c\bar x$, purché $\vartheta > 0$, quindi l'algoritmo si muove sempre verso flussi di base di costo minore.

Nel caso in cui $\vartheta = \vartheta^+$, un arco $(r,s)$ di $T$, oppure lo stesso $(p,q)$ di $L$ si satura e va in $U$, mentre se $\vartheta = \vartheta^-$, un arco $(r,s)$ di $T$, oppure lo stesso $(p,q)$ di $U$ si svuota e va in $L$. Infine, se $\vartheta = 0$, significa che il flusso è degenere e l'algoritmo effettua il cambio di base, ma ovviamente il flusso rimane invariato.
\end{dimostrazione}

\section{Flusso di costo minimo su reti non capacitate} \label{par:reti-mcfnoncap}

Un caso particolare del problema del flusso di costo minimo è rappresentato dal caso in cui gli archi della rete abbiano capacità superiore infinita ($u_{ij} = +\infty$). In questo caso la rete si dice essere non capacitata e il problema può essere espresso togliendo i vincoli di limite superiore al flusso dal \eqref{eq:mcf}:
\begin{equation} \label{eq:mcfnoncap}
\sistema{
	\min\ c x \\
	E x = b \\
	x \geqslant 0
}
\end{equation}

Le basi per il problema \eqref{eq:mcfnoncap} sono formate da alberi di copertura, come mostrato con i teoremi \ref{teo:rangoE} e \ref{teo:rangoE2}, quindi per individuare una base è sufficiente bipartizionare l'insieme degli archi in $T$ e $L$. Il flusso di base si calcola facilmente tramite le equazioni già viste:
$$ \bar x_L = 0 \qquad \bar x_T = E_T^{-1} b $$
Una soluzione di base è quindi ammissibile se $x_T \geqslant 0$, e \textsc{non} degenere se $x_T > 0$ (esattamente come specificato per $\bar y$ al paragrafo \ref{par:pl-algebra-solbase}). Il problema duale del \eqref{eq:mcfnoncap} è il seguente:
\begin{equation} \label{eq:potenzialenoncap}
\sistema{
	\max\ \pi b \\
	\trasp E \pi \leqslant c
}
\end{equation}
e rimangono valide le prime equazioni delle \eqref{eq:potenzialebase} e \eqref{eq:potenzialebaseesplicito}: $\bar\pi = \big(\trasp E_T\big)^{-1} c_T$, ammissibile se $c_L \geqslant \trasp E_L \bar\pi \ \Longleftrightarrow\ \crid \geqslant 0,\ \forall (i,j) \in L$ (condizioni di Bellman per il problema non capacitato) e \textsc{non} degenere se $\crid > 0$.

I teoremi più importanti per il problema capacitato possono essere riformulati in maniera più semplice per quello non capacitato. In particolare:

\begin{teorema}[Bellman per reti non capacitate] \label{teo:bellmannoncap}
Data una base $(T,L)$, dove $T$ è un albero di copertura del grafo, che genera un flusso di base ammissibile, se, calcolato il potenziale di base, esso risulta ammissibile, cioè $\crid \geqslant 0,\ \forall (i,j) \in L$, allora il flusso è ottimo.
\end{teorema}

\subsection{Algoritmo del simplesso per flussi su reti non capacitate} \label{par:reti-mcf-simplessononcap}

L'algoritmo è praticamente uguale al problema capacitato, con l'unica differenza che le basi hanno una forma diversa e, inoltre, bisogna avere l'accortezza di gestire il caso in cui il costo ottimo sia illimitato inferiormente, proprio perché il flusso non è limitato superiormente. Il procedimento è descritto di seguito, il passo aggiuntivo rispetto al problema capacitato è il 5.

\begin{enumerate}
\item Calcolare il flusso e il potenziale di base $\bar x$ e $\bar\pi$ utilizzando le equazioni del paragrafo precedente.
\item Se $\bar\pi$ verifica le condizioni di Bellman per reti non capacitate, allora \textsc{stop $\Longrightarrow\ \bar x$ è ottimo}. Altrimenti:
\item Selezionare come arco entrante $(p,q)$ il primo, in ordine lessicografico, tra quelli in $L$ che violano le condizioni di Bellman.
\item Considerare il ciclo orientato $\C$ che $(p,q)$ forma con gli archi presenti in $T$, di verso concorde a $(p,q)$. Partizionare $\C = \C^+ \cup \C^-$, dove $\C^+$ è formato dagli archi di $\C$ di verso concorde ad esso e $\C^-$ da quelli di verso discorde.
\item Se $\C^- = \emptyset$ (non ci sono archi di verso discorde), allora \textsc{stop $\Longrightarrow\ \min\ cx = -\infty$}. Altrimenti:
\item Calcolare il valore $\displaystyle \vartheta = \min_{(i,j) \in \C^-} \bar x_{ij}$.
\item Selezionare come arco uscente $(r,s)$ il primo, in ordine lessicografico, tra quelli in $\C^-$ tali che $\bar x_{ij} = \vartheta$.
\item Aggiornare il flusso e il potenziale in base alla seguente regola:
$$
\bar x_{ij} \leftarrow \sistema[l @{\quad} l]{
	x_{ij} + \vartheta	& \mathrm{se\ } (i,j) \in \C^+ \\
	x_{ij} - \vartheta	& \mathrm{se\ } (i,j) \in \C^- \\
	x_{ij}				& \mathrm{altrimenti}
}
\qquad
\bar\pi_i \leftarrow \sistema[l @{\quad} l]{
	\bar\pi_i				& \mathrm{se\ } i \in N_p \\
	\bar\pi_i + \crid[pq]	& \mathrm{se\ } i \in N_q
}
$$
dove $N_p$ e $N_q$ sono le due componenti connesse di $T$ che si vengono a creare dopo aver tolto l'arco $(p,q)$ da $T$ stesso, con $p \in N_p$ e $q \in N_q$.
\item Effettuare il cambio di base: $T \leftarrow T \cup (p,q) \setminus (r,s)$ e $L \leftarrow L \cup (r,s) \setminus (p,q)$ e ritornare al punto 2.
\end{enumerate}

La dimostrazione di correttezza è ovviamente simile a quella per il problema capacitato e non sarà ripetuta. Un discorso a parte merita di essere fatto per il passo 5 dell'algoritmo. Consideriamo il caso in cui $\C^-$ è vuoto. Allora significa che tutti gli archi in $\C$, compreso $(p,q)$, sono orientati nello stesso verso e su ognuno di essi si può aumentare il flusso di una quantità infinita. Riprendiamo la formula che esprime il valore della funzione obiettivo al variare di $\vartheta$:
$$ cx(\vartheta) = c\bar x + \vartheta \crid[pq] < c\bar x \longrightarrow -\infty$$
dove, in questo caso,
$$ \crid[pq] = \sum_{(i,j) \in \C^+} c_{ij}\ \cancel{ - \sum_{(i,j) \in \C^-} c_{ij}} 
\quad \Longrightarrow \quad
\sum_{(i,j) \in \C^+} c_{ij} < 0 $$
In conclusione si vede che se il costo ottimo è di valore $-\infty$ allora la somma dei costi degli archi sul ciclo è negativa e quindi esiste almeno un arco di costo negativo.
% Di seguito un esempio per chiarire il concetto.

\begin{esempio}
Un problema di assegnamento visto come un problema di flusso di costo minimo:
\begin{center}
\begin{minipage}{0.4\textwidth} \centering \begin{tabular}{r|*5c}
$c_{ij}$ &  1 &  2 &  3 &  4 &  5 \\ 
	\hline 
	 1 & 56 & 48 & 61 & 47 & 49 \\ 
	 2 & 55 & 49 & 60 & 48 & 52 \\ 
	 3 & 54 & 46 & 62 & 44 & 53 \\ 
	 4 & 49 & 48 & 66 & 41 & 47 \\
	 5 & 45 & 53 & 60 & 55 & 48 \\ 
\end{tabular} \end{minipage}
\hspace{1cm}
\begin{minipage}{0.4\textwidth} \centering \begin{tikzpicture}
[>=latex',auto,node distance=3cm and 4cm,thick,bend angle=20,on grid,pin distance=5pt]

\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
\tikzstyle{every pin}=[inner sep=0pt,draw=none]
\tikzstyle{every pin edge}=[semithick]

\foreach \n/\a in {1/A, 2/B, 3/C, 4/D, 5/E} {
	\node[pin=left:-1] (\n) at (0,-\n) {$\n$};
	\node[pin=right:1] (\n') at (3,-\n) {$\a$};
}

\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

\foreach \x in {1,2,...,5} {
	\foreach \y in {1,2,...,5} {
		\path (\x) edge (\y');
	}
}

\node[draw=none,anchor=south,inner sep=0pt] at (1.5,-1) {$c_{ij}$};
\end{tikzpicture} \end{minipage}
\end{center}
\end{esempio}


