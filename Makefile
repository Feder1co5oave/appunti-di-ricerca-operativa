# inserire qui il percorso dell'eseguibile git
GIT = \cygwin\bin\git

# inserire qui il percorso di pdflatex (lasciare inalterato se � presente nel PATH)
TEX = pdflatex

SHELL = cmd
TEXFLAGS = --quiet -synctex=1 -interaction=nonstopmode
PDFTK = pdftk/pdftk
MAIN_TEX = appunti-master
VERSION = $(shell $(GIT) log -1 --abbrev=8 --pretty=format:%h)


# MAIN TARGET

$(MAIN_TEX).pdf : $(MAIN_TEX).tex gitHeadInfo.gin *.aux
	$(TEX) $(TEXFLAGS) $(MAIN_TEX).tex
	@echo


# FAKE TARGETS

.PHONY : clean distclean dist crypt distcrypt ini

dist : appunti-$(VERSION).pdf

distcrypt : appunti-$(VERSION)-crypt.pdf

clean : distclean
	- del *.log *.aux *.bbl *.blg *.out *.synctex.gz *.toc *.thm

distclean :
	- del *.pdf *.dvi *.ps

ini : gitHeadInfo.gin
	copy /Y gitinfo-hook .git\hooks\post-commit
	copy /Y gitinfo-hook .git\hooks\post-checkout
	copy /Y gitinfo-hook .git\hooks\post-merge


# FILE TARGETS

appunti-$(VERSION).pdf : $(MAIN_TEX).pdf
	copy $(MAIN_TEX).pdf appunti-$(VERSION).pdf

appunti-$(VERSION)-crypt.pdf : appunti-$(VERSION).pdf
	$(PDFTK) appunti-$(VERSION).pdf output appunti-$(VERSION)-crypt.pdf encrypt_128bit allow printing

gitHeadInfo.gin :
	- $(GIT) log -1 --date=short --abbrev=8 --pretty=format:"\usepackage[shash={%%h},lhash={%%H},authname={%%an},authemail={%%ae},authsdate={%%ad},authidate={%%ai},authudate={%%at},commname={%%an},commemail={%%ae},commsdate={%%ad},commidate={%%ai},commudate={%%at},refnames={%%d}]{gitsetinfo}" HEAD > gitHeadInfo.gin

$(MAIN_TEX).tex : copertina.tex pl.tex reti.tex ;

copertina.tex : ;

pl.tex : ;

reti.tex : mcf.tex sp.tex mfmc.tex ;

mcf.tex: ;

sp.tex : ;

mfmc.tex : ;

*.aux : ;
