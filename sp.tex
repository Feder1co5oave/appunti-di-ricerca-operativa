\section{Cammini minimi} \label{par:reti-camminiminimi}

Preso un grafo orientato $G = (N,A)$ ai cui archi sono associati dei costi $c_{ij}$, vogliamo trovare il cammino orientato di costo minimo da un nodo fissato $r$ chiamato ``radice'' a ogni altro nodo del grafo, dove il costo associato al cammino orientato è dato dalla somma dei costi associati ai singoli archi che lo compongono.

Come vedremo, se i costi sono tutti non negativi e il grafo è connesso allora un cammino di costo minimo esiste sempre. In altre condizioni non è detto che esso esista, perché potrebbe avere un costo non limitato inferiormente oppure, nel caso il grafo non sia connesso, potrebbe non esistere \emph{nessun} cammino dalla radice a un certo nodo.

Usiamo questo grafo di esempio (a destra è riportata la sua lista delle adiacenze):

\begin{center}

	\begin{minipage}{.5\textwidth} \centering
	\begin{tikzpicture}
	[>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
		
		\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
		
		\node (1) [on chain,ultra thick] {1};
		
		\foreach \i	in {2,...,6}
			\node (\i) [on chain] {\i};
		
		\node (7) at (0,0) {7};
		
		\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
		\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
	
		\path (1) 	edge node {15} (2)
					edge node {20} (7)
					edge node [swap] {6} (6);
		\path (2)	edge node {10} (3)
					edge node {8} (7);
		\path (3)	edge node {15} (4);
		\path (5)	edge node {12} (4)
					edge node {5} (7);
		\path (6)	edge node {18} (7)
					edge node {8} (5);
		\path (7)	edge node {14} (3)
					edge node {9} (4);
	\end{tikzpicture}
	\end{minipage}
	\hfill
	\begin{minipage}{.4\textwidth} \centering \footnotesize
	\begin{verbatim}
	Numero di nodi: 7
	Radice: 1
	Adiacenze:
	Nodo 1 -> [2(15), 6(6), 7(20)]
	Nodo 2 -> [3(10), 7(8)]
	Nodo 3 -> [4(15)]
	Nodo 4 -> []
	Nodo 5 -> [4(12), 7(5)]
	Nodo 6 -> [5(8), 7(18)]
	Nodo 7 -> [3(14), 4(9)]
	\end{verbatim}
	\end{minipage}
\end{center}

Il problema dei cammini minimi può essere formulato come un problema di flusso di costo minimo non capacitato:
\begin{equation} \label{eq:spcomemcf}
	\sistema{
		\min\ cx \\
		Ex = b \\
		x \geqslant 0
	}
	\qquad
	b_i = \sistema[l @{\quad} l]{
		- (n-1)	& i = r \\
		1		& i \neq r
	}
\end{equation}
\begin{center} \begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
	
	\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
	\tikzstyle{every pin}=[inner sep=0pt,draw=none]
	\tikzstyle{every pin edge}=[semithick]

	\node (1) [on chain,pin=180:-6] {1};
	\node (2) [on chain,pin=120:1] {2};
	\node (3) [on chain,pin=60:1] {3};
	\node (4) [on chain,pin=0:1] {4};
	\node (5) [on chain,pin=-60:1] {5};
	\node (6) [on chain,pin=-120:1] {6};
	\node (7) [pin=-30:1] at (0,0) {7};
	
	\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
	\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

	\path (1) 	edge node {15} (2)
				edge node {20} (7)
				edge node [swap] {6} (6);
	\path (2)	edge node {10} (3)
				edge node {8} (7);
	\path (3)	edge node {15} (4);
	\path (5)	edge node {12} (4)
				edge node {5} (7);
	\path (6)	edge node {18} (7)
				edge node {8} (5);
	\path (7)	edge node {14} (3)
				edge node {9} (4);
\end{tikzpicture} \end{center}

Ricordiamo che le basi per i problemi di flusso non capacitati sono formate da alberi di copertura e la condizione di ammissibilità è $\bar x \geqslant 0$, che per un problema formulato nel modo descritto nella \eqref{eq:spcomemcf} equivale a dire che $T$ è un albero di copertura \emph{orientato} di radice $r$, ovvero ogni nodo ha un solo arco entrante e, poiché il bilancio di tutti i nodi, tranne $r$, è 1, i flussi di base sono sempre non degeneri ($\bar x > 0$). In virtù di questo possiamo sfruttare il \thref{teo:bellmanter} (nella versione per reti non capacitate):
%, che ci dice che la condizione necessaria e sufficiente perché il flusso $\bar x$ sia ottimo è che il potenziale di base sia ammissibile (vedi \ref{par:reti-mcfnoncap}).

\begin{teorema}[Bellman ter per cammini minimi]
Dato un albero di copertura $T$ orientato di radice $r$, esso è un albero dei cammini minimi se e solo se il potenziale $\bar\pi$ associato a $T$ verifica le condizioni di Bellman:
$$ c_{ij} + \bar\pi_i - \bar\pi_j = \crid \geqslant 0 \quad \forall (i,j) \notin T $$
\end{teorema}

Supponiamo, come abbiamo già fatto, di fissare il potenziale di un nodo e scegliamo di farlo per la radice: $\bar\pi_r = 0$. Allora $\bar\pi_i$ è uguale al costo del cammino orientato in $T$ da $r$ a $i$.

\subsection{Algoritmo del simplesso per cammini}

Presentiamo qui l'algoritmo del simplesso per cammini, parente stretto di quello presentato al paragrafo \ref{par:reti-mcf-simplessononcap} che si avvale di tutte le semplificazioni descritte precedentemente. In particolare non ha bisogno di regole anticiclo, visto che tutti i flussi di base sono non degeneri. Ha ovviamente bisogno di una base di partenza $B = (T,L)$ ammissibile.

\begin{enumerate}
\item Calcolare il potenziale di base $\bar\pi = \big(\trasp E_T\big)^{-1} c_T$ associato all'albero, fissando prima $\bar\pi_r = 0$. Calcolare inoltre i costi ridotti per ogni arco in $L$: $\crid = c_{ij} - \bar\pi_j + \bar\pi_i$.
\item Se $\crid \geqslant 0$ per ogni arco in $L$, allora \textsc{stop $\Longrightarrow\ T$ è un albero dei cammini minimi}. Altrimenti:
\item Scegliere come arco entrante $(p,q)$ uno tra quelli in $L$ tali che $\crid[pq] < 0$.
\item Considerare il ciclo orientato $\C$ che $(p,q)$ forma con gli archi presenti in $T$, di verso concorde a $(p,q)$. Partizionare $\C = \C^+ \cup \C^-$, dove $\C^+$ è formato dagli archi di $\C$ di verso concorde ad esso e $\C^-$ da quelli di verso discorde.
\item Se $\C^- = \emptyset$, allora \textsc{stop $\Longrightarrow\ $ l'albero dei cammini minimi non esiste e $\C$ è un ciclo di costo negativo}. Altrimenti:
\item Selezionare come arco uscente $(s,q)$ l'unico arco di $T$ entrante in $q$.
\item Aggiornare i potenziali e i costi ridotti in base alla seguente regola:
$$ \bar\pi_i \leftarrow \sistema[l @{\quad} l]{
	\bar\pi				& \mathrm{se\ } i \in N_p \\
	\bar\pi + \crid[pq]	& \mathrm{se\ } i \in N_q
}
\qquad
\crid[ij] \leftarrow \sistema[l @{\quad} l]{
	\crid[ij]				& \mathrm{se\ } (i,j \in N_q) \vee (i,j \in N_q) \\
	\crid[ij] - \crid[pq]	& \mathrm{se\ } i \in N_p, j \in N_q \\
	\crid[ij] + \crid[pq]	& \mathrm{se\ } i \in N_q, j \in N_p
} $$
dove $N_p$ e $N_q$ sono le due componenti connesse di $T$ che si vengono a creare dopo aver tolto l'arco $(s,q)$ da $T$ stesso, con $p,r \in N_p$ e $q \in N_q$.
\item Effettuare il cambio di base: $T \leftarrow T \cup (p,q) \setminus (i,q)$ (aggiornare consistentemente anche $L$) e ritornare al punto 2.	
\end{enumerate}

\subsection{Algoritmo di Dijkstra}

L'algoritmo di Dijkstra, omonimo del suo inventore, è nato appositamente per risolvere il problema dei cammini minimi in un grafo orientato. La sua limitazione principale è che funziona solo se i costi degli archi sono non negativi. In caso contrario non è garantito che restituisca un risultato corretto.

Ad ogni iterazione l'algoritmo mantiene un albero di copertura memorizzato in un vettore dei predecessori $p$. $p_i$ è il nodo predecessore di $i$, cioè nell'albero è presente l'arco $(i,p_i)$. Inoltre utilizza un vettore dei potenziali $\pi$, che abbiamo già visto rappresenta il costo del cammino dalla radice al nodo, se fissiamo $\pi_r = 0$.

Non ha bisogno di una base di partenza in quanto l'algoritmo costruisce a priori un albero di copertura composto da archi fittizi di costo infinito e mano a mano li sostituisce con archi reali del grafo, costruendo la soluzione. Per farlo, inizializza i vettori $p$ e $\pi$ nel seguente modo:
\begin{equation} \label{eq:dijkstrainit}
p_i = \sistema[l @{\quad} l]{
	0	& i = r \\
	-1	& i \neq r
}
\qquad
\pi_i = \sistema[l @{\quad} l]{
	0		& i = r \\
	+\infty	& i \neq r
}
\qquad
Q = \{r\}
\end{equation}

L'algoritmo mantiene un insieme $Q$ detto ``di respiro'' che contiene i nodi dai quali escono gli archi che potenzialmente violano le condizioni di Bellman. Esso viene inizializzato in modo da contenere il solo nodo radice. Ad ogni iterazione viene estratto un nodo da $Q$ di potenziale minimo e ne vengono esaminati gli archi uscenti. Per ognuno $(i,j)$ di questi, se $\pi_j > \pi_i + c_{ij}$ significa che il cammino da $r$ a $j$ passando per $i$ costa meno del cammino attualmente presente nell'albero, sia esso di archi reali o fittizi. L'algoritmo termina quando $Q = \emptyset$ e restituisce l'albero dei cammini minimi.

\begin{enumerate}
\item Inizializzare $p$, $\pi$ e $Q$ come specificato in \eqref{eq:dijkstrainit}.
\item Se $Q = \emptyset$ allora \textsc{stop $\Longrightarrow\ p$ è l'albero dei cammini minimi}. Altrimenti:
\item Estrarre da $Q$ il nodo $i$ di potenziale $\pi_i$ minimo.
\item Per ogni arco $(i,j)$ uscente da $i$, se $\pi_j > \pi_i + c_{ij}$ aggiornare $\pi_j \leftarrow \pi_i + c_{ij}$ e $p_j = i$ e inserire $j$ in $Q$, se non è già presente. Ritornare al punto 2.
\end{enumerate}

%\begin{teorema}
%Se $c_{ij} \geqslant 0$ per ogni arco $(i,j)$, allora l'algoritmo di Dijkstra trova un albero dei cammini minimi dopo $n$ iterazioni.
%\end{teorema}
%\paragraph*{Dimostrazione}

\begin{esempio}
Applichiamo l'algoritmo di Dijkstra all'esempio riportato al paragrafo \ref{par:reti-camminiminimi}:

\begin{enumerate} \setlength{\parskip}{0pt}
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 & -1 & -1 & -1 & -1 & -1 & -1 & )  \\
			\pi = ( &  0 & +\infty & +\infty & +\infty & +\infty & +\infty & +\infty & ) \\
			 Q = \{ &  1 &    &    &    &    &    &    & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}
		[scale=.8,>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
			\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
			\node (1) [on chain,label=left:0] {1};
			\foreach \i	in {2,...,6}
				\node (\i) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {\i};
			\node (7) [label=90:$+\infty$] at (0,0) {7};
		\end{tikzpicture}
	\end{minipage}

\resetcounter[3]
\item $i = 1 \qquad \{(1,2), (1,6), (1,7)\}$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 & -1 & -1 & -1 &  1 &  1 & )  \\
			\pi = ( &  0 & 15 & +\infty & +\infty & +\infty & \phantom 06 & 20 & ) \\
			 Q = \{ &    &  2 &    &    &    &  6 &  7 & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}
		[scale=.8,>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
			\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
			\node (1) [on chain,ultra thick,label=left:0] {1};
			\node (2) [on chain,label=(-\tikzchaincount*60+240):15] {2};
			\node (3) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {3};
			\node (4) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {4};
			\node (5) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {5};
			\node (6) [on chain,label=(-\tikzchaincount*60+240):6] {6};
			\node (7) [label=90:20] at (0,0) {7};
			
			\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
			\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
		
			\path (1) edge (2) edge (7) edge (6);
		\end{tikzpicture}
	\end{minipage}
\hrule \resetcounter[3]
\item $i = 6 \qquad \{(6,5), (6,7)\}$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 & -1 & -1 &  6 &  1 &  1 & )  \\
			\pi = ( &  0 & 15 & +\infty & +\infty & 14 & \phantom 06 & 20 & ) \\
			 Q = \{ &    &  2 &    &    &  5 &    &  7 & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}
		[scale=.8,>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
			\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
			\node (1) [on chain,label=left:0] {1};
			\node (2) [on chain,label=(-\tikzchaincount*60+240):15] {2};
			\node (3) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {3};
			\node (4) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {4};
			\node (5) [on chain,label=(-\tikzchaincount*60+240):14] {5};
			\node (6) [on chain,ultra thick,label=(-\tikzchaincount*60+240):6] {6};
			\node (7) [label=90:20] at (0,0) {7};
			
			\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
			\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
		
			\path (1) edge (2) edge (7) edge (6);
			\path (6) edge (5);
		\end{tikzpicture}
	\end{minipage}
\hrule \resetcounter[3]
\item $i = 5 \qquad \{(5,4), (5,7)\}$
\item 
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 & -1 &  5 &  6 &  1 &  5 & )  \\
			\pi = ( &  0 & 15 & +\infty & 26 & 14 & \phantom 06 & 19 & ) \\
			 Q = \{ &    &  2 &    &  4 &    &    &  7 & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}
		[scale=.8,>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
			\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
			\node (1) [on chain,label=left:0] {1};
			\node (2) [on chain,label=(-\tikzchaincount*60+240):15] {2};
			\node (3) [on chain,label=(-\tikzchaincount*60+240):$+\infty$] {3};
			\node (4) [on chain,label=(-\tikzchaincount*60+240):26] {4};
			\node (5) [on chain,ultra thick,label=(-\tikzchaincount*60+240):14] {5};
			\node (6) [on chain,label=(-\tikzchaincount*60+240):6] {6};
			\node (7) [label=90:19] at (0,0) {7};
			
			\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
			\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
		
			\path (1) edge (2) edge (6);
			\path (6) edge (5);
			\path (5) edge (4) edge (7);
		\end{tikzpicture}
	\end{minipage}
\hrule \resetcounter[3]
\item $i = 2 \qquad \{(2,3), (2,7)\}$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 &  2 &  5 &  6 &  1 &  5 & )  \\
			\pi = ( &  0 & 15 & 25 & 26 & 14 & \phantom 06 & 19 & ) \\
			 Q = \{ &    &    &  3 &  4 &    &    &  7 & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}
		[scale=.8,>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
			\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]
			\node (1) [on chain,label=left:0] {1};
			\node (2) [on chain,ultra thick,label=(-\tikzchaincount*60+240):15] {2};
			\node (3) [on chain,label=(-\tikzchaincount*60+240):25] {3};
			\node (4) [on chain,label=(-\tikzchaincount*60+240):26] {4};
			\node (5) [on chain,label=(-\tikzchaincount*60+240):14] {5};
			\node (6) [on chain,label=(-\tikzchaincount*60+240):6] {6};
			\node (7) [label=90:19] at (0,0) {7};
			
			\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
			\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]
		
			\path (1) edge (2) edge (6);
			\path (6) edge (5);
			\path (5) edge (4) edge (7);
			\path (2) edge (3);
		\end{tikzpicture}
	\end{minipage}
\hrule \resetcounter[3]
\item $i = 7 \qquad \{(7,3), (7,4)\}$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 &  2 &  5 &  6 &  1 &  5 & )  \\
			\pi = ( &  0 & 15 & 25 & 26 & 14 & \phantom 06 & 19 & ) \\
			 Q = \{ &    &    &  3 &  4 &    &    &    & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth} \centering invariato \end{minipage}
\hrule \resetcounter[3]
\item $i = 3 \qquad \{(3,4)\}$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 &  2 &  5 &  6 &  1 &  5 & )  \\
			\pi = ( &  0 & 15 & 25 & 26 & 14 & \phantom 06 & 19 & ) \\
			 Q = \{ &    &    &    &  4 &    &    &    & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth} \centering invariato \end{minipage}
\hrule \resetcounter[3]
\item $i = 4 \qquad \emptyset$
\item
	\begin{minipage}{0.45\textwidth}
		$ \begin{array}{r @{\;} *6{r @{,\;}} r @{\;} l}
		%	 V = \{ &  1 &  2 &  3 &  4 &  5 &  6 &  7 & \} \\
			  p = ( &  0 &  1 &  2 &  5 &  6 &  1 &  5 & )  \\
			\pi = ( &  0 & 15 & 25 & 26 & 14 & \phantom 06 & 19 & ) \\
			 Q = \{ &    &    &    &    &    &    &    & \}
		\end{array}$
	\end{minipage}
	\begin{minipage}{0.49\textwidth} \centering invariato \end{minipage}
\hrule \resetcounter[2]
\item $Q = \emptyset \Longrightarrow$ \textsc{stop} (albero dei cammini minimi completato)
\end{enumerate}

La soluzione del problema dei cammini minimi è quindi la seguente, dove sono evidenziati l'albero dei cammini minimi e i pesi dei singoli archi.

\begin{center}
\begin{tikzpicture}
[>=latex',auto,node distance=3cm,thick,bend angle=20,pin distance=5pt,start chain=circle placed {at=(-\tikzchaincount*60+240 : 3)}]
	
	\tikzstyle{every node}=[shape=circle,draw=black,text=black,inner sep=3pt]

	\node (1) [on chain,ultra thick] {1};
	\foreach \i	in {2,...,6}
		\node (\i) [on chain] {\i};
	\node (7) at (0,0) {7};
	
	\tikzstyle{every node}=[draw=none,inner sep=1pt,font=\footnotesize,shape=ellipse]
	\tikzstyle{every edge}+=[->,shorten <=1pt,shorten >=1pt]

	\path (1) 	edge node {15} (2)
				edge node [swap] {6} (6);
	\path (2)	edge node {10} (3);
	\path (5)	edge node {12} (4)
				edge node {5} (7);
	\path (6)	edge node {8} (5);
\end{tikzpicture} 
\end{center}
\end{esempio}